<?php 

    $json = file_get_contents("php://input"); 
    $objJsonArray = json_decode($json,true);
    
    //la opcion del 0 al 2 es para sugerir 3 rangos de valores para cada uno de los tipos de reto
    
    //trae el total de consumo de energia para el dia anterior
if(isset($objJsonArray["tipoReto"]) && ((int) $objJsonArray["tipoReto"]) == 0){
        $codigo = $objJsonArray["codigo"];
        $con = mysqli_connect("fdb14.awardspace.net","2218323_ef","adminDE0","2218323_ef") or die ("no puedo conectar con db");
	$query_maxidCONSUMOENERGIA = "select MAX(idCONSUMOENERGIA) from CONSUMOENERGIA where codContador='".$codigo."'";
					
	$query = "select totalConsumoDia from CONSUMOENERGIA where idCONSUMOENERGIA=(".$query_maxidCONSUMOENERGIA.")";
	
	$resultado = mysqli_query($con,$query) or die("error: ".mysqli_error($con));
	$listado = mysqli_fetch_array($resultado);
	$consumoDia = doubleval($listado[0]);	

	$consumoDia = round($consumoDia,2);//redondeamos a dos cifras decimales
	$promedioDown=round(($consumoDia*0.9) + mt_rand()/mt_getrandmax() * ($consumoDia - $consumoDia*0.9),2);
	$promedioUp = round($consumoDia + mt_rand()/mt_getrandmax() * ($consumoDia*1.1 - $consumoDia),2);	

	$arrayResponse = array('dc'=>array($promedioDown,$consumoDia,$promedioUp));
	echo json_encode($arrayResponse);
}
    //trae la sumatoria de los ultimos 7 dias del total de consumo de energia 
if(isset($objJsonArray["tipoReto"]) && ((int) $objJsonArray["tipoReto"]) == 1){
        $codigo = $objJsonArray["codigo"];
        $con = mysqli_connect("fdb14.awardspace.net","2218323_ef","adminDE0","2218323_ef") or die ("no puedo conectar con db");
	        
	$queryLastWeek = "SELECT totalConsumoDia FROM `CONSUMOENERGIA` WHERE codContador = '".$codigo."' ORDER BY idCONSUMOENERGIA DESC LIMIT 7";	
	$resultado = mysqli_query($con,$queryLastWeek) or die("error: ".mysqli_error($con));
	$weekDays = array();	
	$sumatoriaWeekDays = 0;
	$i = 0;	
	while ($listado = mysqli_fetch_array($resultado, MYSQL_NUM)){				
		$weekDays[$i] = doubleval($listado[0]);		
		$sumatoriaWeekDays = $sumatoriaWeekDays + $weekDays[$i];
		$i++;
	}

	//$promedioWeek = round($sumatoriaWeekDays/7,2);
        $promedioWeek = round($sumatoriaWeekDays,2);
	$promedioDownWeek=round(($promedioWeek*0.9) + mt_rand()/mt_getrandmax() * ($promedioWeek - $promedioWeek*0.9),2);
	$promedioUpWeek = round($promedioWeek + mt_rand()/mt_getrandmax() * ($promedioWeek*1.1 - $promedioWeek),2);	
	
	$arrayResponse = array('wc'=>array($promedioDownWeek,$promedioWeek,$promedioUpWeek));
	echo json_encode($arrayResponse);
}
 //trae la sumatoria de los ultimos 30 dias del total de consumo de energia 
if(isset($objJsonArray["tipoReto"]) && ((int) $objJsonArray["tipoReto"]) == 2){
        $codigo = $objJsonArray["codigo"]; 
        $con = mysqli_connect("fdb14.awardspace.net","2218323_ef","adminDE0","2218323_ef") or die ("no puedo conectar con db");       
        
	$queryLastThirtyDays = "SELECT totalConsumoDia FROM `CONSUMOENERGIA` WHERE codContador = '".$codigo."' ORDER BY idCONSUMOENERGIA DESC LIMIT 30";	
	$resultado = mysqli_query($con,$queryLastThirtyDays) or die("error: ".mysqli_error($con));
	$ThirtyDays = array();	
	$sumatoriaThirtyDays = 0;
	$i = 0;	
	while ($listado = mysqli_fetch_array($resultado, MYSQL_NUM)){				
		$ThirtyDays[$i] = doubleval($listado[0]);			
		$sumatoriaThirtyDays = $sumatoriaThirtyDays + $ThirtyDays[$i];
		$i++;
	}
	
	$promedioThirtyDays = round($sumatoriaThirtyDays,2);

	$promedioDownThirtyDays=round(($promedioThirtyDays*0.9) + mt_rand()/mt_getrandmax() * ($promedioThirtyDays - $promedioThirtyDays*0.9),2);

	$promedioUpThirtyDays = round($promedioThirtyDays + mt_rand()/mt_getrandmax() * ($promedioThirtyDays*1.1 - $promedioThirtyDays),2);	
	
	$arrayResponse = array('mc'=>array($promedioDownThirtyDays,$promedioThirtyDays,$promedioUpThirtyDays));
	echo json_encode($arrayResponse);

}

//opcion para el service de tipo Diario (consulta y retorna la sumatoria del total de consumo de un dia)
if( isset($objJsonArray["tipoReto"]) && ((int) $objJsonArray["tipoReto"]) == 3){ 
	$codigoMedidor = $objJsonArray["codigo"];	
        $con = mysqli_connect("fdb14.awardspace.net","2218323_ef","adminDE0","2218323_ef") or die ("no puedo conectar con db");
	$qMaxidCONSUMOENERG = "select MAX(idCONSUMOENERGIA) from CONSUMOENERGIA where codContador = '".$codigoMedidor."'";	
	$querytotalConsumoDia = "select totalConsumoDia from CONSUMOENERGIA where idCONSUMOENERGIA =(".$qMaxidCONSUMOENERG.")";
	$my_rta = mysqli_query($con,$querytotalConsumoDia) or die("error: ".mysqli_error($con));
	$listado = mysqli_fetch_array($my_rta);
	if($my_rta){
		echo json_encode( array('vFDC'=>$listado[0]) );	//vFDC: value Finish Daily Challenge
	}
}

//opcion para el service de tipo Semanal (consulta y retorna la sumatoria del total de consumo de 7 dias)
if( isset($objJsonArray["tipoReto"]) && ((int) $objJsonArray["tipoReto"]) == 4){ 
	

        $codigo = $objJsonArray["codigo"];
        $con = mysqli_connect("fdb14.awardspace.net","2218323_ef","adminDE0","2218323_ef") or die ("no puedo conectar con db");        
	$queryLastWeek = "SELECT totalConsumoDia FROM `CONSUMOENERGIA` WHERE codContador = '".$codigo."' ORDER BY idCONSUMOENERGIA DESC LIMIT 7";	
	$resultado = mysqli_query($con,$queryLastWeek) or die("error: ".mysqli_error($con));
	$weekDays = array();	
	$sumatoriaWeekDays = 0;
	$i = 0;	
	while ($listado = mysqli_fetch_array($resultado, MYSQL_NUM)){				
		$weekDays[$i] = doubleval($listado[0]);		
		$sumatoriaWeekDays = $sumatoriaWeekDays + $weekDays[$i];
		$i++;
	}

	$consumoSemanal = round($sumatoriaWeekDays,2);
		
	echo json_encode(array('vFWC'=>$consumoSemanal)); //vFWC: value Finish Weekly Challenge
}

//opcion para el service de tipo Mensual (consulta y retorna la sumatoria del total de consumo de 30 dias)
if( isset($objJsonArray["tipoReto"]) && ((int) $objJsonArray["tipoReto"]) == 5){ 
	$codigo = $objJsonArray["codigo"];
    $con = mysqli_connect("fdb14.awardspace.net","2218323_ef","adminDE0","2218323_ef") or die ("no puedo conectar con db");        
	$queryLastThirtyDays = "SELECT totalConsumoDia FROM `CONSUMOENERGIA` WHERE codContador = '".$codigo."' ORDER BY idCONSUMOENERGIA DESC LIMIT 30";	
	$resultado = mysqli_query($con,$queryLastThirtyDays) or die("error: ".mysqli_error($con));
	$ThirtyDays = array();	
	$sumatoriaThirtyDays = 0;
	$i = 0;	
	while ($listado = mysqli_fetch_array($resultado, MYSQL_NUM)){				
		$ThirtyDays[$i] = doubleval($listado[0]);		
		$sumatoriaThirtyDays = $sumatoriaThirtyDays + $ThirtyDays[$i];
		$i++;
	}

	$consumoSemanal = round($sumatoriaThirtyDays,2);
		
	echo json_encode(array('vFMC'=>$consumoSemanal)); //vFMC: value Finish Monthly Challenge
}



?>