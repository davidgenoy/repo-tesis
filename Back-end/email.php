<?php 

    ini_set('display_errors',1); 
    error_reporting(E_ALL);
    
	include("PHPExcel/IOFactory.php");
    
	$hostname = '{imap.gmail.com:993/imap/ssl}INBOX';
	$username = 'dsenergyfriend@gmail.com';
	$pass = 'adminDE0';
	$attachments = array();
	$folderPath = "./tempExcel/";
	$excelPath = array();

	function getmsg($mbox,$mid) {   

        global $charset, $htmlmsg, $plainmsg, $attachments, $from, $to, $folderPath;

        $htmlmsg = $plainmsg = $charset = '';
        $attachments = array();
        // HEADER
        $h = imap_headerinfo($mbox,imap_msgno($mbox,$mid));
        // BODY
        $s = imap_fetchstructure($mbox,$mid,FT_UID);
        if (!$s->parts)  // simple
        	getpart($mbox,$mid,$s,0);  // pass 0 as part-number
        
        else {  // multipart: cycle through each part
        	//echo "cantidad: ".count($s->parts)."<br>";
        	foreach ($s->parts as $partno0=>$p){
        		//echo "partno0: ".$partno0."<br>";
        		//echo "p: ".$p->subtype."<br>";
        		//echo "pasa: ".($partno0+1)."<br>";
          		getpart($mbox,$mid,$p,($partno0+1));
        	}
        }
    }

    function getpart($mbox,$mid,$p,$partno) {
        // $partno = '1', '2', '2.1', '2.1.3', etc for multipart, 0 if simple
        global $htmlmsg,$plainmsg,$charset,$attachments,$last_mail_id,$newstr,$c,$ok,$folderPath,
        		$excelPath;

        $data = ($partno) ? imap_fetchbody($mbox,imap_msgno($mbox,$mid),$partno) : imap_body($mbox,imap_msgno($mbox,$mid));  // simple
        //echo "partno recibido: ".$partno."<br>";
        //echo "p recibido: ".$p->subtype."<br>";
        //var_dump($data);
        if ($p->encoding==4)
        $data = quoted_printable_decode($data);
        else if ($p->encoding==3)
        $data = base64_decode($data);//aqui estan los datos del excel
        // PARAMETERS    // get all parameters, like charset, filenames of attachments, etc.
        $params = array();
        if ($p->parameters)
        foreach ($p->parameters as $x)
            $params[strtolower($x->attribute)] = $x->value;
        if (isset($p->dparameters))
        foreach ($p->dparameters as $x)
            $params[strtolower($x->attribute)] = $x->value;

        // ATTACHMENT    // Any part with a filename is an attachment,
        // so an attached text file (type 0) is not mistaken as the message.
        if (isset($params['filename']) || isset($params['name'])) {
           // filename may be given as 'Filename' or 'Name' or both
        $filename = ($params['filename'])? $params['filename'] : $params['name'];
        //echo "tengo: ".$filename;
        // filename may be encoded, so see imap_mime_header_decode()
         $attachments[$filename] = $data;  // this is a problem if two files have same name
        //store id and filename in array
        }

        //save the attachments in the directory
        foreach( $attachments as $key => $val){
          $fname = $key;
          $fp = fopen("$folderPath/$fname","w");
          fwrite($fp, $val);
          fclose($fp);
          $excelPath[$key] = $folderPath.$fname;
        }

            // TEXT
            if ($p->type==0 && $data) {
            // Messages may be split in different parts because of inline attachments,   // so append parts together with blank row.
            if (strtolower($p->subtype)=='plain')
                $plainmsg .= trim($data)."\n\n";
            else
                //preg_match_all('/<img[^>]+>/i',$data, $result);
                $htmlmsg .= $data."<br><br>";
                $charset = $params['charset'];  // assume all parts are same charset
            }

        // There are no PHP functions to parse embedded messages, so this just appends the raw source to the main message.
        else if ($p->type==2 && $data) {
        $plainmsg .= $data."\n\n";
        }
        // SUBPART RECURSION
        if (isset($p->parts)) {
        foreach ($p->parts as $partno0=>$p2)
            getpart($mbox,$mid,$p2,$partno.'.'.($partno0+1));  // 1.2, 1.2.1, etc.
        }

        //return ;
    }

	$imapStream= imap_open($hostname,$username,$pass) or die("no se pudo conectar:".imap_last_error());
	$ids = imap_search($imapStream, 'UNSEEN', SE_UID);//retornamos todos los uid (en un array) de cada correo NUEVO, los cuales no cambian en el tiempo. (No se reorganizan numericamente asi eliminemos algun correo"!)
	//$ids retornara FALSE cuando no hayan msjs nuevos, se evalua diferente de FALSE !

    $msjOut="";

    if($ids) { 
        
        for ($i=0; $i < count($ids) ; $i++) { 
          getmsg($imapStream,$ids[$i]);
        }
        //conexion BD
        $con = mysqli_connect("localhost","root","adminDE","EnergyFriendDB") or die ("no puedo conectar con db");
        /////////////

        foreach ($excelPath as $key => $value) {

        	$objPHPExcel = PHPExcel_IOFactory::load($value);
        	$objHoja = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        	            
			for($i=10; $i<=count($objHoja)-9; $i++){
                
                $arrayKwh = array();

                $time = strtotime($objHoja[$i]['B']);
                $newformat = date('Y-m-d',$time);             
                
                //$dia = intval(substr($newformat, 8, 2));
                $mes = intval(substr($newformat, 5, 2));
                $anio = intval(substr($newformat, 0, 4));
                
                if(($mes == 1) || ($mes == 01)){

                    $mes = 12;
                    $anio = $anio - 1;
                }else{

                    $mes = $mes-1;
                }

                //$fechaPIC = "2014-".$mes."-27";
                $fechaPIC = $anio."-".$mes."-27";

                $arrayKwh[0] = $objHoja[$i]['E'];
                $arrayKwh[1] = $objHoja[$i]['G'];
                $arrayKwh[2] = $objHoja[$i]['J'];
                $arrayKwh[3] = $objHoja[$i]['L'];
                $arrayKwh[4] = $objHoja[$i]['N'];
                $arrayKwh[5] = $objHoja[$i]['O'];
                $arrayKwh[6] = $objHoja[$i]['Q'];
                $arrayKwh[7] = $objHoja[$i]['R'];
                $arrayKwh[8] = $objHoja[$i]['T'];
                $arrayKwh[9] = $objHoja[$i]['X'];
                $arrayKwh[10] = $objHoja[$i]['Z'];
                $arrayKwh[11] = $objHoja[$i]['AB'];
                $arrayKwh[12] = $objHoja[$i]['AE'];
                $arrayKwh[13] = $objHoja[$i]['AG'];
                $arrayKwh[14] = $objHoja[$i]['AJ'];
                $arrayKwh[15] = $objHoja[$i]['AL'];
                $arrayKwh[16] = $objHoja[$i]['AN'];
                $arrayKwh[17] = $objHoja[$i]['AP'];
                $arrayKwh[18] = $objHoja[$i]['AR'];
                $arrayKwh[19] = $objHoja[$i]['AT'];
                $arrayKwh[20] = $objHoja[$i]['AW'];
                $arrayKwh[21] = $objHoja[$i]['AZ'];
                $arrayKwh[22] = $objHoja[$i]['BC'];
                $arrayKwh[23] = $objHoja[$i]['BF'];  
                //validamos que no venga un dato con valor NA, sino lo ponemos a 0    
                for ($k=0; $k < count($arrayKwh); $k++) { 
                    if($arrayKwh[$k] == "NA"){
                        $arrayKwh[$k] = 0.00;
                    }
                }
                                     
                //eliminamos los * del total y obtenemos el valor del double
                $totalDia = doubleval(str_replace("*", "", $objHoja[$i]['BI']));
                
                $query = "INSERT INTO CONSUMOENERGIA VALUES (NULL,'".$objHoja[6]['AC']."','".$newformat."','".$fechaPIC."',".$totalDia.")";
                $resultado = mysqli_query($con,$query) or die("error en Uno: ".mysqli_error($con));
                
                //capturtamos el id del ultimo registro
                $queryidCONSUMOENERGIA="select MAX(idCONSUMOENERGIA) from CONSUMOENERGIA";
                $resultado2 = mysqli_query($con,$queryidCONSUMOENERGIA) or die("error: ".mysqli_error($con));
                $listado = mysqli_fetch_array($resultado2);
                $idCONSUMOENERGIA =  (int) $listado[0];            
                
                for ($j=0; $j < count($arrayKwh); $j++) { 
                    
                    $queryInsertHH = "INSERT INTO CONSUMOHH VALUES (NULL,".$arrayKwh[$j].",".$idCONSUMOENERGIA.")"; 
                    $resultado3 = mysqli_query($con,$queryInsertHH) or die("error: ".mysqli_error($con));
                }                    
                
                if($resultado && $resultado2 && $resultado3){                    
                    $msjOut = "datos insertados correctamente!";
                }else{                    
                    $msjOut = "error al insertar los datos!!!";
                }
				
				unset($arrayKwh);
			}
        }
            echo $msjOut;
    }else {//cuando $ids retorna FALSE
         echo "Lo sentimos, no tienes mensajes nuevos...<br>";
    }	
	  	
?>
