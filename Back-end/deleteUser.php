<?php
	ini_set('display_errors',1); 
    error_reporting(E_ALL); 
	
	$json = file_get_contents("php://input"); 
    $objJsonArray = json_decode($json,true);
    
    $email = $objJsonArray["email"];    
    $codigo = $objJsonArray["codigo"];
	$con = mysqli_connect("fdb14.awardspace.net","2218323_ef","adminDE0","2218323_ef") or die ("no puedo conectar con db");
	$query_idCONSUMOENERGIA = "SELECT idCONSUMOENERGIA FROM CONSUMOENERGIA where codContador='".$codigo."' LIMIT 0,1";
    $query = "select count(*) from USUARIO where idConsumoEnergiaFK = (".$query_idCONSUMOENERGIA.")";
    $resultado = mysqli_query($con,$query) or die("error: ".mysqli_error($con));
	$listado = mysqli_fetch_array($resultado);	
	$cuenta = (int) $listado[0];

	if($cuenta == 1){//si entra es el unico usuario asociado al medidor. Ademas de eliminar al usuario, debemos eliminar los retos asociados a dicho codigo (si alguna vez creo uno).

		//consultamos si alguna vez el usuario creo un reto (idRetoFK != NULL)
		$query_idRetoFK = "select idRetoFK from USUARIO where idConsumoEnergiaFK=(".$query_idCONSUMOENERGIA.")";
		$idRetoFK = mysqli_query($con,$query_idRetoFK) or die("error: ".mysqli_error($con));
		$result_idRetoFK = mysqli_fetch_array($idRetoFK);

		$query_deleteUser = "delete from USUARIO where idConsumoEnergiaFK=(".$query_idCONSUMOENERGIA.")";
		
		if(is_null($result_idRetoFK[0])){ //si entra, nunca creo un reto, solo eliminamos al usuario
			
			$mysqli_query = mysqli_query($con,$query_deleteUser) or die("error: ".mysqli_error($con));
			//$result_idRetoFK = mysqli_fetch_array($idRetoFK);
			if($mysqli_query)			
				echo json_encode(array("out" => 't'));
			
		}else{//si entra, si creo alguna vez un reto, eliminamos al usuario y luego el Reto con el idReto.
			$mysqli_query = mysqli_query($con,$query_deleteUser) or die("error: ".mysqli_error($con));
			
			if($mysqli_query){//si elimino al usuario entonces eliminamos el Reto:
				$query_deleteChallenge = "delete from RETO where idReto=(".(int) $result_idRetoFK[0].")";
				$mysqli_query = mysqli_query($con,$query_deleteChallenge) or die("error: ".mysqli_error($con));
				if($mysqli_query)
					echo json_encode(array("out" => 't'));
			}

		}
	}

	if($cuenta > 1){//aqui entra cuando hay por lo menos dos usuarios reistrados en la BD con ese codigo de medidor

		//delegamos los permisos de admin de Grupo a otro usuario (si el tipo de usuario es 1)
		//capturamos el segundo usuario registrado en la BD

		//primero deberos verificar si se trata de un usuario tipo 1 o 2
		$query_typeUser = "select tipoUsuario from USUARIO where idConsumoEnergiaFK=(".$query_idCONSUMOENERGIA.") AND correoFcbk = '".$email."'";
		$msquery = mysqli_query($con,$query_typeUser) or die("error: ".mysqli_error($con));		
		$lista = mysqli_fetch_array($msquery);
		$typeUser = (int) $lista[0];

		//capturamos el segundo usuario registrado en la BD
		$query_twoUser = "SELECT idUSUARIO FROM USUARIO where idConsumoEnergiaFK = (".$query_idCONSUMOENERGIA.") LIMIT 1,1";
		$msqueryUserID = mysqli_query($con,$query_twoUser) or die("error: ".mysqli_error($con));		
		$lista = mysqli_fetch_array($msqueryUserID);
		$userTwoId = (int) $lista[0];
		//////////////////////////////////////

		$query_deleteUserOne = "delete from USUARIO where correoFcbk='".$email."'";	
		if($typeUser == 1){//delegamos los permisos de admin de Grupo a otro usuario, y eliminamos al usuario

			$query_updateTU = "UPDATE USUARIO SET tipoUsuario = 1 where idUSUARIO = '".$userTwoId."'";
			$result = mysqli_query($con,$query_updateTU) or die("error: ".mysqli_error($con));	
			if($result){				
				mysqli_query($con,$query_deleteUserOne) or die("error: ".mysqli_error($con));	
				echo json_encode(array("out" => 't'));
			}

		}else{//si entra, typeUser vale 2, solo eliminamos al usuario			
			mysqli_query($con,$query_deleteUserOne) or die("error: ".mysqli_error($con));
			echo json_encode(array("out" => 't'));
		}

	}
	
?>