package co.edu.unicauca.energyfriend.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import co.edu.unicauca.energyfriend.R;
import co.edu.unicauca.energyfriend.adapters.TabAdapter;
import co.edu.unicauca.energyfriend.fragments.DailyFragment;
import co.edu.unicauca.energyfriend.fragments.MonthlyFragment;
import co.edu.unicauca.energyfriend.fragments.WeeklyFragment;
import co.edu.unicauca.energyfriend.net.ManageRequest;
import co.edu.unicauca.energyfriend.preferences.AppUtil;
import co.edu.unicauca.energyfriend.receivers.ResponseReceiver;
import co.edu.unicauca.energyfriend.services.DailyChallengeIntentService;
import co.edu.unicauca.energyfriend.services.MonthlyChallengeIntentService;
import co.edu.unicauca.energyfriend.services.WeeklyChallengeIntentService;
import de.hdodenhof.circleimageview.CircleImageView;
import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;

//import android.support.v4.view.GravityCompat;

public class MenuActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, ViewPager.OnPageChangeListener, MaterialTabListener {

    //constantes
    public static final String TAG = MenuActivity.class.getSimpleName();
    public static final String URL_TYPEUSER = "http://energyfriend.atwebpages.com/typeUserRequest.php";
    public static final String URL_DELETE_USER = "http://energyfriend.atwebpages.com/deleteUser.php";
    public static final String UNIVERSAL_TAG = "stack";

    //Atributos
    CircleImageView circleImageView;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    NavigationView navigationView;
    TextView textViewUserName, textViewUserEmail;
    String userNameFcbk, urlImage, userEmail;
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    AlertDialog.Builder alertDialog;
    TabAdapter tabAdapter;
    DailyFragment dailyFragment;
    WeeklyFragment weeklyFragment;
    MonthlyFragment monthlyFragment;
    List<Fragment> data;
    ViewPager viewPager;
    MaterialTabHost materialTabHost;
    int[] icons = {R.drawable.daily_tab, R.drawable.weekly_tab, R.drawable.monthly_tab};
    boolean bandera0 = true, bandera1 = true, bandera2 = true;

    IntentFilter filter; // Filtro de acciones
    ResponseReceiver receiver; // Broadcast receiver
    Intent intentChallengeD,intentChallengeW,intentChallengeM;

    //variables de invitaciones
    CallbackManager callbackManagerInviteUsers;
    private static final String appLinkUrl = "https://fb.me/209864492781060";
    private static final String previewImageUrl = "https://goo.gl/lGNee5";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        FacebookSdk.sdkInitialize(this);
        Log.d(UNIVERSAL_TAG,"ESTOY en MenuActivity");
        sharedPreferences = getSharedPreferences(AppUtil.PREFERENCE_NAME, MODE_PRIVATE);
        editor = getSharedPreferences(AppUtil.PREFERENCE_NAME, MODE_PRIVATE).edit();
        navigationView = (NavigationView) findViewById(R.id.navigation_view);

        //determinar el tipo de usuario para segun ello, cargar el respectivo menu de opciones
        if(sharedPreferences.getInt(AppUtil.TYPE_USER,-1) == 1)
            navigationView.inflateMenu(R.menu.nav_menu);//cargamos el menu del Admin de Grupo
        else {
            navigationView.inflateMenu(R.menu.nav_menu_invited); //cargamos el menu del Invitado
            executeServiceInvited();//ademas, debemos ejecutar el service que monitorea cada 15 minutos los retos Diario, Semanal y Mensual
        }

        //espacio para configurar la opcion de enviar invitaciones
        callbackManagerInviteUsers = CallbackManager.Factory.create();
        //////////////////////////////////////////////////////////
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_toogle);// fijamos ícono del drawer toggle
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);//desplegamos el toogle
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        //navigationView = (NavigationView) findViewById(R.id.navigation_view);
        materialTabHost = (MaterialTabHost) findViewById(R.id.materialTabHost);
        View headerLayout = navigationView.getHeaderView(0);//obtenemos el layout del header del NavigationView
        textViewUserName = (TextView) headerLayout.findViewById(R.id.txt_nav_header);
        textViewUserEmail = (TextView) headerLayout.findViewById(R.id.txt_email_user);
        circleImageView = (CircleImageView) headerLayout.findViewById(R.id.profile_image);

        urlImage = sharedPreferences.getString(AppUtil.USER_IMAGE_URL, "");
        userNameFcbk = sharedPreferences.getString(AppUtil.USER_NAME, "");
        userEmail = sharedPreferences.getString(AppUtil.USER_EMAIL, "");
        Picasso.with(this).load(Uri.parse(urlImage)).into(circleImageView);//procesa la url desde internet y la inserta en el circleImageView
        textViewUserName.setText(userNameFcbk);
        textViewUserEmail.setText(userEmail);
        navigationView.setNavigationItemSelectedListener(this);

        initAllFragments();
        data = new ArrayList<>();
        data.add(dailyFragment);
        data.add(weeklyFragment);
        data.add(monthlyFragment);

        tabAdapter = new TabAdapter(getSupportFragmentManager(), data, icons, this);
        viewPager.setAdapter(tabAdapter);
        viewPager.addOnPageChangeListener(this);

        // Añadir 3 pestañas y asignarles un icono y escucha
        for (int i = 0; i < tabAdapter.getCount(); i++) {
            //materialTabHost.setAccentColor(ContextCompat.getColor(this,R.color.colorAccentTab));
            materialTabHost.addTab(
                    materialTabHost.newTab()
                            .setIcon(tabAdapter.getIcon(i))
                            .setTabListener(this)
            );//metodos de MaterialTabListener
        }

        //esto vigila que solo la primera vez que se usa la App se muestre el DrawerLayout
        if (sharedPreferences.getBoolean(AppUtil.OPEN_DRAWER_LAYOUT, true)) {
            //drawerLayout.openDrawer(GravityCompat.START);
            drawerLayout.openDrawer(Gravity.LEFT);
            editor.putBoolean(AppUtil.OPEN_DRAWER_LAYOUT, false);
            editor.commit();
        }



    }

    private void initAllFragments() {//estos fragment muestran los graficos de consumo (mpandroidchart)
        dailyFragment = new DailyFragment();
        weeklyFragment = new WeeklyFragment();
        monthlyFragment = new MonthlyFragment();
    }

    //opciones del menu lateral (navigationView)
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        //item.setChecked(true);

        switch (item.getItemId()) {
            case R.id.nav_historial_consumo:
                Intent intent = new Intent(this,MenuActivity.class);
                //si ya hay una instancia de esta activity, limpia las q estan por encima de esta y se le pasa el Intent (por medio de  onNewIntent()) a esta activity que ahora esta en el Top del Back Stack
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                //startActivity(new Intent(this, MenuActivity.class));
                break;
            case R.id.nav_log_out:
                showAlertDialog(this);
                break;
            case R.id.nav_drop_out:
                showDropOutDialog(this);
                break;
            case R.id.nav_challenge:
                Intent intentt = new Intent(this,ChallengeActivity.class);//es la clase que gestiona los 3 tipos de reto y a esta opcion SOLO INGRESA EL ADMIN DE GRUPO, no se da el caso para el usuario de tipo invitado
                intentt.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//si ya hay una instancia de esta activity, limpia las q estan por encima de esta y se le pasa el Intent (por medio de  onNewIntent()) a esta activity que ahora esta en el Top del Back Stack
                startActivity(intentt);
                //startActivity(new Intent(this, ChallengeActivity.class));
                break;
            case R.id.nav_invite_friends:
                inviteUsers();
                break;
            case R.id.nav_consumo_pesos:
                Intent intenttt = new Intent(this,ConsumptionActivity.class);
                intenttt.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//si ya hay una instancia de esta activity, limpia las q estan por encima de esta y se le pasa el Intent (por medio de  onNewIntent()) a esta activity que ahora esta en el Top del Back Stack
                startActivity(intenttt);
                //startActivity(new Intent(this, ConsumptionActivity.class));
                break;
        }
        drawerLayout.closeDrawers();//cierra el drawer despues de seleccionar una opcion
        return true;
    }

    private void showAlertDialog(final Context context) {
        alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("¿Está seguro que desea cerrar la sesión?");
        alertDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                editor.putString(AppUtil.USER_NAME, "");
                editor.putString(AppUtil.USER_EMAIL, "");
                editor.putString(AppUtil.USER_IMAGE_URL, "");
                editor.putBoolean(AppUtil.USER_LOGIN_SESSION, false);
                editor.commit();
                LoginManager.getInstance().logOut();//cerramos la sesion de Fcbk en nuestra App
                Intent intents = new Intent(context,MainActivity.class);
                intents.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//si ya hay una instancia de esta activity, limpia las q estan por encima de esta y se le pasa el Intent (por medio de  onNewIntent()) a esta activity que ahora esta en el Top del Back Stack
                startActivity(intents);
                //startActivity(new Intent(context, MainActivity.class));
            }
        });
        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }

    private void showDropOutDialog(final Context context){
        alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("¿Está seguro que desea eliminar esta cuenta definitivamente?");
        alertDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ManageRequest.makeDropOutRequest(context, URL_DELETE_USER, new ManageRequest.VolleyDataResponseListener() {
                    @Override
                    public void onResponseDataObject(JSONObject response) {
                        try {
                            if(response.getString("out").equals("t")){
                                clearAllDataUser();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorDataObject(String message) {

                    }
                });
            }
        });

        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }

    private void inviteUsers() {
        final AppInviteDialog appInviteDialog = new AppInviteDialog(this);

        if (AccessToken.getCurrentAccessToken() == null) {
            Toast.makeText(getBaseContext(), "AccessToken null", Toast.LENGTH_SHORT).show();
        } else {
            FacebookCallback<AppInviteDialog.Result> facebookCallback = new FacebookCallback<AppInviteDialog.Result>() {
                @Override
                public void onSuccess(AppInviteDialog.Result result) {
                    Toast.makeText(getBaseContext(), "Invitaciones enviadas correctamente", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onCancel() {
                    Toast.makeText(getBaseContext(), "Se ha cancelado la acción", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(FacebookException error) {
                    Toast.makeText(getBaseContext(), "error al enviar invitaciones", Toast.LENGTH_SHORT).show();

                }
            };

            if (appInviteDialog.canShow()) {
                AppInviteContent.Builder content = new AppInviteContent.Builder();
                content.setApplinkUrl(appLinkUrl);
                content.setPreviewImageUrl(previewImageUrl);
                AppInviteContent appInviteContent = content.build();
                appInviteDialog.registerCallback(callbackManagerInviteUsers, facebookCallback);
                appInviteDialog.show(this, appInviteContent);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManagerInviteUsers.onActivityResult(requestCode, resultCode, data);
    }


    //metodos de addOnPageChangeListener (viewPager)
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        switch (position){
            case 0:
                if(bandera0){
                    Log.d(TAG,"entro bandera 0");
                    bandera0 = false;
                    dailyFragment.validar();
                }
                break;

            case 1:
                if(bandera1){
                    Log.d(TAG,"entro bandera 1");
                    bandera1 = false;
                    weeklyFragment.validar();
                }
                break;
            case 2:
                if(bandera2){
                    Log.d(TAG,"entro bandera 2");
                    bandera2 = false;
                }
                break;
        }
        materialTabHost.setSelectedNavigationItem(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
    //////////////////////fin//////////////////

    //metodos de MaterialTabListener
    @Override
    public void onTabSelected(MaterialTab tab) {

        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabReselected(MaterialTab tab) {

    }

    @Override
    public void onTabUnselected(MaterialTab tab) {

    }
    /////////////////////fin///////////////////

    ////////Menu del Toolbar///////

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //drawerLayout.openDrawer(GravityCompat.START);
                drawerLayout.openDrawer(Gravity.LEFT);
                break;
        }
        return true;
    }
    //////////////////////////////
    private void clearAllDataUser(){
        LoginManager.getInstance().logOut();//cerramos la sesion de Fcbk en nuestra App

        //limpiamos todas las variables de SharedPreferences
        editor.putBoolean(AppUtil.USER_LOGIN_SESSION,false);
        editor.putString(AppUtil.USER_NAME,"");
        editor.putString(AppUtil.USER_EMAIL,"");
        editor.putString(AppUtil.USER_IMAGE_URL,"");
        editor.putString(AppUtil.VALOR,"");

        editor.putInt(AppUtil.TYPE_USER, -1);
        editor.putBoolean(AppUtil.OLD_USER,false);
        editor.putBoolean(AppUtil.OPEN_DRAWER_LAYOUT, true);

        editor.putString(AppUtil.DAILY_CHALLENGE_DATE_START, "");
        editor.putString(AppUtil.DAILY_CHALLENGE_DATE_FINISH, "");
        editor.putFloat(AppUtil.DAILY_CHALLENGE_VALUE, -1);

        editor.putString(AppUtil.WEEKLY_CHALLENGE_DATE_START,"");
        editor.putString(AppUtil.WEEKLY_CHALLENGE_DATE_FINISH,"");
        editor.putFloat(AppUtil.WEEKLY_CHALLENGE_VALUE,-1);

        editor.putString(AppUtil.MONTHLY_CHALLENGE_DATE_START,"");
        //editor.putString(AppUtil.MONTHLY_CHALLENGE_DATE_FINISH,"");
        editor.putFloat(AppUtil.MONTHLY_CHALLENGE_VALUE,-1);

        editor.commit();
        Toast.makeText(this,"Usuario eliminado con éxito!",Toast.LENGTH_SHORT).show();

        //va de ultimo
        Intent intentl = new Intent(getApplicationContext(),MainActivity.class);
        intentl.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//si ya hay una instancia de esta activity, limpia las q estan por encima de esta y se le pasa el Intent (por medio de  onNewIntent()) a esta activity que ahora esta en el Top del Back Stack
        startActivity(intentl);
        //startActivity(new Intent(getApplicationContext(), MainActivity.class));
    }

    //metodo para ejecutar el service para el usuario de tipo 2 (Invitado)
    public void executeServiceInvited(){
        // Filtro de acciones que serán alertadas
        filter = new IntentFilter(AppUtil.ACTION_SERVICE_DAILY_INVITED);
        filter.addAction(AppUtil.ACTION_SERVICE_WEEKLY_INVITED);
        filter.addAction(AppUtil.ACTION_SERVICE_MONTHLY_INVITED);

        // Crear un nuevo ResponseReceiver
        receiver = new ResponseReceiver(this);// Broadcast receiver que recibe las emisiones desde los servicios
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver,filter); // Registrar el receiver y su filtro

        //arrancamos el service Diario
        intentChallengeD = new Intent(this, DailyChallengeIntentService.class);
        intentChallengeD.setAction(AppUtil.ACTION_SERVICE_DAILY_INVITED);
        intentChallengeD.putExtra(AppUtil.EXTRA_SERVICE_DAILY_INVITED, DailyChallengeIntentService.DAILY_SERVICE_DELAY_INVITED);
        startService(intentChallengeD);//se ejecuta el service

        //arrancamos el service Semanal
        intentChallengeW = new Intent(this, WeeklyChallengeIntentService.class);
        intentChallengeW.setAction(AppUtil.ACTION_SERVICE_WEEKLY_INVITED);

        intentChallengeW.putExtra(AppUtil.EXTRA_SERVICE_WEEKLY_INVITED,WeeklyChallengeIntentService.WEEKLY_SERVICE_DELAY_INVITED);
        startService(intentChallengeW);

        //arrancamos el service Mensual
        intentChallengeM = new Intent(this, MonthlyChallengeIntentService.class);
        intentChallengeM.setAction(AppUtil.ACTION_SERVICE_MONTHLY_INVITED);

        intentChallengeM.putExtra(AppUtil.EXTRA_SERVICE_MONTHLY_INVITED, MonthlyChallengeIntentService.MONTHLY_SERVICE_DELAY_INVITED);
        startService(intentChallengeM);
    }

    //escuchador cuando el usuario presiona el boton de atras
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this,MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//si ya hay una instancia de esta activity, limpia las q estan por encima de esta y se le pasa el Intent (por medio de  onNewIntent()) a esta activity que ahora esta en el Top del Back Stack
        intent.putExtra("EXIT",true);
        startActivity(intent);
        super.onBackPressed();
    }
}
