package co.edu.unicauca.energyfriend.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import co.edu.unicauca.energyfriend.R;
import co.edu.unicauca.energyfriend.net.ManageRequest;
import co.edu.unicauca.energyfriend.preferences.AppUtil;

/**
 * A simple {@link Fragment} subclass.
 */
public class WeeklyFragment extends Fragment {

    //Atributos
    public static final String TAG = WeeklyFragment.class.getSimpleName();

    BarChart barChart;
    List<BarEntry> entries;
    // String[] etiquetas = new String[]{"Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"};
    BarDataSet dataSet;
    BarData barData;

    //atributos para dibujar el reto (si existe)
    YAxis yAxis;
    LimitLine limitLine;
    float limitValue, promSemana = 0.0f;
    //////////
    String[] etiquetas=new String[]{
            "Semana 1: ",
            "Semana 2: ",
            "Semana 3: ",
            "Semana 4: ",
            "Semana 5: ",
            "Semana 6: ",
            "Semana 7: ",
            "Semana 8: ",
    };
    Double[] varDoble=new  Double[8];
    View v;

    public WeeklyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_weekly, null);
        //validar();
        return v;
    }

    public void validar() {

        ManageRequest.makeJsonRequestData(getContext(), AppUtil.URLWeekly, new ManageRequest.VolleyDataResponseListener() {
            @Override
            public void onErrorDataObject(String message) {
                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponseDataObject(JSONObject response) {
                Log.d(TAG,"me llego: "+response);
                String cadena = "";
                String fixCadena = "";
                int index = 0;
                int newIndex = 0;

                JSONArray jsonArray = null;
                try {
                    jsonArray = response.getJSONArray("out");
                    //////agregado por Stiven
                    limitValue = (float) response.getDouble("vWC");
                    /////////////////////////
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(TAG, "entroa JSONException");
                }


                for (int i = 0; i < etiquetas.length; i++) {
                    //JSONArray jsonArray = response.getJSONArray("out");
                    //System.out.println("Bandera David "+jsonArray);
                    //System.out.println("Bandera David "+jsonArray.getString(1));

                    try {
                        if (jsonArray.getString(i).equals("null")) {
                            varDoble[i] = 0.0;
                            //System.out.println("Bandera "+jsonArray.getString(i));
                        } else {
                            varDoble[i] = Double.parseDouble(jsonArray.getString(i));
                        }

                    } catch (Exception e) {
                        varDoble[i] = 0.0;
                        System.out.println("catch en weekly " + e);
                    }

                    System.out.println("Bandera " + varDoble[i] + " " + i);

                    // System.out.println("VALOR DE RESPONSE  " + response.getJSONObject("out"));

                }
                graficar();

            }
        });
    }
    private void graficar(){

        barChart = (BarChart) v.findViewById(R.id.bar_chart);
        entries = new ArrayList<>();
        for (int i=0; i<etiquetas.length;i++){
            entries.add(new BarEntry(varDoble[i].floatValue(), i));
            // System.out.println("DATOS DEL VARDOUBLE en for graficar "+varDoble[i]);
        }

        dataSet = new BarDataSet(entries,"Consumo Semanal en Kwh");
        //dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        barData = new BarData(etiquetas,dataSet);
        barChart.setData(barData);
        barChart.animateXY(3000,3000);
        barChart.setDescription("Historial de Consumo Semanal");

        if (limitValue != 0) {
            Log.d(TAG, "si es diferente de cero");
            promedioSemana();
            yAxis = barChart.getAxisLeft();
            limitLine = new LimitLine(promSemana, "Reto para esta semana: " + limitValue + " Kwh");
            limitLine.setLineWidth(2f);
            limitLine.setTextSize(12f);
            yAxis.addLimitLine(limitLine);
        }

        barChart.invalidate();
    }

    public void promedioSemana() {
        for(int k=0; k<8; k++){
            //promDia = (float) ( (promDia + varDoble[k])/24 );
            promSemana = (float) ( (promSemana + varDoble[k]) );
        }
        promSemana = promSemana/8;
        Log.d(TAG,"la suma de la semana es: "+ promSemana);
    }


}
