package co.edu.unicauca.energyfriend.preferences;

/**
 * Created by Administrador on 24/10/2016.
 */
public class AppUtil {
    public static final String PREFERENCE_NAME = "preferencias";
    public static final String USER_NAME = "username";
    public static final String USER_EMAIL = "useremail";
    public static final String USER_LOGIN_SESSION = "userlogin";
    public static final String USER_IMAGE_URL = "userimageurl";
    public static final String TYPE_USER = "typeuser";

    public static final String VALOR = "valor";
    public static final String URLDaily = "http://energyfriend.atwebpages.com/dailyRequestBDFinal.php";
    public static final String URLWeekly = "http://energyfriend.atwebpages.com/weeklyRequestBDFinal.php";
    public static final String URLMonthly ="http://energyfriend.atwebpages.com/montlyRequestBDFinal.php";

    public static final String OLD_USER ="olduser";
    public static final String OPEN_DRAWER_LAYOUT ="drawer";

    public static final String DAILY_CHALLENGE_DATE_START = "dcds";
    public static final String DAILY_CHALLENGE_DATE_FINISH = "dcdf";
    public static final String DAILY_CHALLENGE_VALUE = "dcv";

    public static final String WEEKLY_CHALLENGE_DATE_START ="wcds";
    public static final String WEEKLY_CHALLENGE_DATE_FINISH ="wcdf";
    public static final String WEEKLY_CHALLENGE_VALUE ="wcv";

    public static final String MONTHLY_CHALLENGE_DATE_START ="mcds";
    public static final String MONTHLY_CHALLENGE_DATE_FINISH ="mcdf";
    public static final String MONTHLY_CHALLENGE_VALUE ="mcv";


    //constantes para los services (Acciones y parametros Extra)
    public static final String ACTION_SERVICE_DAILY = "ASD";
    public static final String ACTION_SERVICE_DAILY_INVITED = "ASDI";
    public static final String EXTRA_SERVICE_DAILY = "ESD";
    public static final String EXTRA_SERVICE_DAILY_INVITED = "ESDI";
    public static final String SERV_DAILY_INVITED_HAS_NOT_BEEN_SEEN = "SDIHNBS";

    public static final String ACTION_SERVICE_WEEKLY = "ASW";
    public static final String ACTION_SERVICE_WEEKLY_INVITED = "ASWI";
    public static final String EXTRA_SERVICE_WEEKLY = "ESW";
    public static final String EXTRA_SERVICE_WEEKLY_INVITED = "ESWI";
    public static final String SERV_WEEKLY_INVITED_HAS_NOT_BEEN_SEEN = "SWIHNBS";

    public static final String ACTION_SERVICE_MONTHLY = "ASM";
    public static final String ACTION_SERVICE_MONTHLY_INVITED = "ASMI";
    public static final String EXTRA_SERVICE_MONTHLY = "ESM";
    public static final String EXTRA_SERVICE_MONTHLY_INVITED = "ESMI";
    public static final String SERV_MONTHLY_INVITED_HAS_NOT_BEEN_SEEN = "SMIHNBS";
}
