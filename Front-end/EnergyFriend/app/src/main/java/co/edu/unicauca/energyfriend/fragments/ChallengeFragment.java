package co.edu.unicauca.energyfriend.fragments;


import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import co.edu.unicauca.energyfriend.R;
import co.edu.unicauca.energyfriend.net.ManageRequest;
import co.edu.unicauca.energyfriend.preferences.AppUtil;
import co.edu.unicauca.energyfriend.receivers.ResponseReceiver;
import co.edu.unicauca.energyfriend.services.DailyChallengeIntentService;
import co.edu.unicauca.energyfriend.services.MonthlyChallengeIntentService;
import co.edu.unicauca.energyfriend.services.WeeklyChallengeIntentService;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChallengeFragment extends Fragment implements View.OnClickListener {

    //atributos
    public static final String TAG = ChallengeFragment.class.getSimpleName(); // etiqueta para mensajes de consola
    public static final String BUNDLE_CHALLENGE = "my_bundle";
    public static final String TITLE_CHALLENGE_DAILY = "Seleccione el reto Diario";
    public static final String TITLE_CHALLENGE_WEEKLY = "Seleccione el reto Semanal";
    public static final String TITLE_CHALLENGE_MONTHLY = "Seleccione el reto Mensual";
    public static final int POSITION_CHALLENGE_DAILY = 0;
    public static final int POSITION_CHALLENGE_WEEKLY = 1;
    public static final int POSITION_CHALLENGE_MONTHLY = 2;
    public static final String CHALLENGE = "tipoReto";
    public static final String CODIGO_CONTADOR = "codigo";
    public static final String DATE_CREATE_CHALLENGE = "fcreacion";
    public static final String VALUE_CHALLENGE = "vchallenge";
    public static final String URL_FLAG = "http://energyfriend.atwebpages.com/manageFlagXChallenge.php";
    public static final String URL = "http://energyfriend.atwebpages.com/managmentChallenge.php";
    public static final String URL_INSERT_CHALLENGE = "http://energyfriend.atwebpages.com/insertChallenge.php";
    public static final String URL_INSERT_WEEKLY_CHALLENGE = "http://energyfriend.atwebpages.com/insertWeeklyChallenge.php";

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    TextView txtTitle,txtCreateDate,txtEndDate,txtValueChallenge;
    RadioButton radioButtonUp, radioButtonValue, radioButtonDown;
    Button btnSetChallenge;
    RadioGroup radioGroup;
    Fragment myfragment;
    View view;
    Intent intentChallenge;
    int positionTab;
    String valorReto;

    IntentFilter filter; // Filtro de acciones
    ResponseReceiver receiver; // Broadcast receiver

    public ChallengeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        positionTab = getArguments().getInt(BUNDLE_CHALLENGE);
    }

    public void init(Context context, Fragment fragment){
        myfragment = fragment;
        ManageRequest.makeChallengeRequest(context, URL, positionTab, new ManageRequest.VolleyChallengeDataListener() {
            @Override
            public void onResponseChallengeData(JSONObject response) {
                Log.d(TAG,"estoy dentro de init y el server respondio: "+response);
                try {
                    switch (positionTab) {
                        case 0:
                            Log.d(TAG,"estoy en el init del case 0");
                            txtTitle.setText(TITLE_CHALLENGE_DAILY);
                            if (response.names().getString(0).equals("dc")) {
                                setValuesOnRadioButtons(response.getJSONArray("dc"));
                            }
                            break;
                        case 1:
                            Log.d(TAG,"estoy en el init del case 1");
                            txtTitle.setText(TITLE_CHALLENGE_WEEKLY);
                            if (response.names().getString(0).equals("wc")) {
                                setValuesOnRadioButtons(response.getJSONArray("wc"));
                            }
                            break;
                        case 2:
                            Log.d(TAG,"estoy en el init del case 2");
                            txtTitle.setText(TITLE_CHALLENGE_MONTHLY);
                            if (response.names().getString(0).equals("mc")) {
                                setValuesOnRadioButtons(response.getJSONArray("mc"));
                            }
                            break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorChallengeData(String message) {

            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        sharedPreferences = getContext().getSharedPreferences(AppUtil.PREFERENCE_NAME, MODE_PRIVATE);
        editor = getContext().getSharedPreferences(AppUtil.PREFERENCE_NAME, MODE_PRIVATE).edit();

        switch (positionTab){

            case 0:
                Log.d(TAG,"pase por onCreateView del case 0!!!");
                if(sharedPreferences.getFloat(AppUtil.DAILY_CHALLENGE_VALUE,-1) != -1 )//verificamos si se ha establecido el reto diario, para NO cargar la vista de los valores sugeridos por la App
                    initViewsChallengeReady(inflater,container);//y asi cargar la fecha de creacion, caducidad y valor
                else
                    initViewsChallenge(inflater,container);
                break;

            case 1:
                Log.d(TAG,"pase por onCreateView del case 1!!!");
                if(sharedPreferences.getFloat(AppUtil.WEEKLY_CHALLENGE_VALUE,-1) != -1)//verificamos si se ha establecido el reto semanal, para NO cargar la vista de los valores sugeridos por la App
                    initViewsChallengeReady(inflater,container);//y asi cargar la fecha de creacion, caducidad y valor
                else
                    initViewsChallenge(inflater,container);
                break;
            case 2:
                Log.d(TAG,"pase por onCreateView del case 2!!!");
                if(sharedPreferences.getFloat(AppUtil.MONTHLY_CHALLENGE_VALUE,-1) != -1)//verificamos si se ha establecido el reto mensual, para NO cargar la vista de los valores sugeridos por la App
                    initViewsChallengeReady(inflater,container);//y asi cargar la fecha de creacion, caducidad y valor
                else
                    initViewsChallenge(inflater,container);
                break;
        }

        return view;
    }
    //este metodo inicializa las referencias a los views del layout y segun el valor de positionTab (pestaña seleccionada) asigna una imagen al boton que establece el reto
    private void initViewsChallenge(LayoutInflater inflater, ViewGroup container) {
        view = inflater.inflate(R.layout.fragment_challenge, container, false);
        txtTitle = (TextView) view.findViewById(R.id.txt_challenge_title);
        radioButtonUp = (RadioButton) view.findViewById(R.id.radio_btn_upvalue);
        radioButtonValue = (RadioButton) view.findViewById(R.id.radio_btn_value);
        radioButtonDown = (RadioButton) view.findViewById(R.id.radio_btn_downvalue);
        btnSetChallenge = (Button) view.findViewById(R.id.btn_set_challenge);
        radioGroup = (RadioGroup) view.findViewById(R.id.radio_group);
        btnSetChallenge.setOnClickListener(this);
        switch (positionTab){
            case 0:
                btnSetChallenge.setCompoundDrawablesWithIntrinsicBounds(0,0, R.drawable.daily_tab,0);//establece una imagen en la parte derecha del boton
                break;
            case 1:
                btnSetChallenge.setCompoundDrawablesWithIntrinsicBounds(0,0, R.drawable.weekly_tab,0);//establece una imagen en la parte derecha del boton
                break;
            case 2:
                btnSetChallenge.setCompoundDrawablesWithIntrinsicBounds(0,0, R.drawable.monthly_tab,0);//establece una imagen en la parte derecha del boton
                break;
        }
    }
    //entra a este metodo cuando ya se ha establecido el reto, segun sea el caso
    private void initViewsChallengeReady(LayoutInflater inflater, ViewGroup container){
        view = inflater.inflate(R.layout.fragment_challenge_ready, container, false);
        txtCreateDate = (TextView) view.findViewById(R.id.txt_create_date);
        txtEndDate = (TextView) view.findViewById(R.id.txt_end_date);
        txtValueChallenge = (TextView) view.findViewById(R.id.txt_value_challenge);

        switch (positionTab){
            case 0:
                txtCreateDate.setText(sharedPreferences.getString(AppUtil.DAILY_CHALLENGE_DATE_START,""));
                txtEndDate.setText(sharedPreferences.getString(AppUtil.DAILY_CHALLENGE_DATE_FINISH,""));
                txtValueChallenge.setText(sharedPreferences.getFloat(AppUtil.DAILY_CHALLENGE_VALUE,-1) + " Kwh");
                break;
            case 1:
                txtCreateDate.setText(sharedPreferences.getString(AppUtil.WEEKLY_CHALLENGE_DATE_START,""));
                txtEndDate.setText(sharedPreferences.getString(AppUtil.WEEKLY_CHALLENGE_DATE_FINISH,""));
                txtValueChallenge.setText(sharedPreferences.getFloat(AppUtil.WEEKLY_CHALLENGE_VALUE,-1) + " Kwh");
                break;
            case 2:
                txtCreateDate.setText(sharedPreferences.getString(AppUtil.MONTHLY_CHALLENGE_DATE_START,""));
                //txtEndDate.setText(sharedPreferences.getString(AppUtil.MONTHLY_CHALLENGE_DATE_FINISH,""));
                txtEndDate.setText("30 días después de la Fecha de Creación");
                txtValueChallenge.setText(sharedPreferences.getFloat(AppUtil.MONTHLY_CHALLENGE_VALUE,-1) + " Kwh");
                break;
        }

    }


    public void setValuesOnRadioButtons(JSONArray jsonArray) {
        try {
            radioButtonUp.setText(jsonArray.getDouble(2)+" Kwh");
            radioButtonValue.setText(jsonArray.getDouble(1)+" Kwh");
            radioButtonDown.setText(jsonArray.getDouble(0)+" Kwh");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        final RadioButton radioButton;
        int selectedId = radioGroup.getCheckedRadioButtonId();// get selected radio button from radioGroup
        radioButton = (RadioButton) view.findViewById(selectedId);// find the radiobutton by returned id
        final String fechaInicial = setDateFormat();
        int positionEmpty = ((String) radioButton.getText()).indexOf(" ");
        valorReto = ((String) radioButton.getText()).substring(0, positionEmpty);

        if(positionTab != 1) {//es para los retos Diario y Mensual solamente
            ManageRequest.makeInsertChallengeRequest(getActivity(), URL_INSERT_CHALLENGE, positionTab, fechaInicial, valorReto, new ManageRequest.VolleyChallengeDataListener() {
                @Override
                public void onResponseChallengeData(JSONObject response) {
                    try {
                        switch (positionTab) {
                            case 0:
                                if (response.names().getString(0).equals("finDC")) {

                                    editor.putString(AppUtil.DAILY_CHALLENGE_DATE_START, fechaInicial);
                                    editor.putString(AppUtil.DAILY_CHALLENGE_DATE_FINISH, response.getString("finDC"));
                                    editor.putFloat(AppUtil.DAILY_CHALLENGE_VALUE, Float.parseFloat(valorReto));
                                    editor.commit();
                                    Toast.makeText(getActivity(), "Reto creado exitosamente!", Toast.LENGTH_SHORT).show();
                                    refreshTab();
                                    executeService(positionTab);

                                }
                                break;
                            case 2:
                                if(response.getString("finMC").equals("t")){
                                    editor.putString(AppUtil.MONTHLY_CHALLENGE_DATE_START, fechaInicial);
                                    //editor.putString(AppUtil.MONTHLY_CHALLENGE_DATE_FINISH, response.getString("finMC"));
                                    editor.putFloat(AppUtil.MONTHLY_CHALLENGE_VALUE, Float.parseFloat(valorReto));
                                    editor.commit();
                                    Toast.makeText(getActivity(), "Reto creado exitosamente!", Toast.LENGTH_SHORT).show();
                                    refreshTab();
                                    executeService(positionTab);
                                }
                                break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onErrorChallengeData(String message) {

                }
            });
        }else{//fue necesario separar la request a la BD para el caso cuando vale 1 xq por alguna razon no permitia cerrar una "}" en el script PHP
            Log.d(TAG,"entro al else");
            valorReto = ((String) radioButton.getText()).substring(0, 5);
            ManageRequest.makeInsertWeeklyChallengeRequest(getActivity(), URL_INSERT_WEEKLY_CHALLENGE, fechaInicial, valorReto, new ManageRequest.VolleyChallengeDataListener() {
                @Override
                public void onResponseChallengeData(JSONObject response) {
                    Log.d(TAG,"me llega: "+response);
                    try {
                        if (response.names().getString(0).equals("finWC")) {
                            editor.putString(AppUtil.WEEKLY_CHALLENGE_DATE_START, fechaInicial);
                            editor.putString(AppUtil.WEEKLY_CHALLENGE_DATE_FINISH, response.getString("finWC"));
                            editor.putFloat(AppUtil.WEEKLY_CHALLENGE_VALUE, Float.parseFloat(valorReto));
                            editor.commit();
                            Toast.makeText(getActivity(), "Reto creado exitosamente!", Toast.LENGTH_SHORT).show();
                            refreshTab();
                            executeService(positionTab);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onErrorChallengeData(String message) {

                }
            });
        }
    }

    public String setDateFormat(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateFormat = simpleDateFormat.format(new Date());
        return dateFormat;
    }

    public void refreshTab(){
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.detach(myfragment);
        ft.attach(myfragment);
        ft.commit();
    }

    public void executeService(int position){
        // Filtro de acciones que serán alertadas
        filter = new IntentFilter(AppUtil.ACTION_SERVICE_DAILY);
        filter.addAction(AppUtil.ACTION_SERVICE_WEEKLY);
        filter.addAction(AppUtil.ACTION_SERVICE_MONTHLY);
        // Crear un nuevo ResponseReceiver
        receiver = new ResponseReceiver(getActivity());// Broadcast receiver que recibe las emisiones desde los servicios
        // Registrar el receiver y su filtro
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver,filter);

        switch (position){
            case 0:
                intentChallenge = new Intent(getContext(), DailyChallengeIntentService.class);
                intentChallenge.setAction(AppUtil.ACTION_SERVICE_DAILY);
                intentChallenge.putExtra(AppUtil.EXTRA_SERVICE_DAILY, new String[]{
                        String.valueOf(sharedPreferences.getFloat(AppUtil.DAILY_CHALLENGE_VALUE,-1)),
                        String.valueOf(DailyChallengeIntentService.DAILY_SERVICE_DELAY)
                });
                getActivity().startService(intentChallenge);//se inicia el service
                break;
            case 1:
                //arrancamos el service
                intentChallenge = new Intent(getContext(), WeeklyChallengeIntentService.class);
                intentChallenge.setAction(AppUtil.ACTION_SERVICE_WEEKLY);
                intentChallenge.putExtra(AppUtil.EXTRA_SERVICE_WEEKLY, new String[]{
                        String.valueOf(sharedPreferences.getFloat(AppUtil.WEEKLY_CHALLENGE_VALUE,-1)),
                        String.valueOf(WeeklyChallengeIntentService.WEEKLY_SERVICE_DELAY)
                });
                getActivity().startService(intentChallenge);
                Log.d("llegadaJson","el reto es: "+intentChallenge.getStringArrayExtra(AppUtil.EXTRA_SERVICE_WEEKLY)[0]);
                break;
            case 2:
                //arrancamos el service
                intentChallenge = new Intent(getContext(), MonthlyChallengeIntentService.class);
                intentChallenge.setAction(AppUtil.ACTION_SERVICE_MONTHLY);
                intentChallenge.putExtra(AppUtil.EXTRA_SERVICE_MONTHLY, new String[]{
                        String.valueOf(sharedPreferences.getFloat(AppUtil.MONTHLY_CHALLENGE_VALUE,-1)),
                        String.valueOf(MonthlyChallengeIntentService.MONTHLY_SERVICE_DELAY)
                });
                getActivity().startService(intentChallenge);
                break;
        }

    }

}
