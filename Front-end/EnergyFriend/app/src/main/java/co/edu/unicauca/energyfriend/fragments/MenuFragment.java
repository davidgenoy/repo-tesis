package co.edu.unicauca.energyfriend.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.squareup.picasso.Picasso;
import co.edu.unicauca.energyfriend.R;
import co.edu.unicauca.energyfriend.preferences.AppUtil;
import de.hdodenhof.circleimageview.CircleImageView;

public class MenuFragment extends Fragment implements View.OnClickListener {

    Context context;
    SharedPreferences sharedPreferences;
    public MenuFragment(Context context) {
    this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_menu, null);
        CircleImageView circleImageView = (CircleImageView) v.findViewById(R.id.profile_image_2);
        sharedPreferences = context.getSharedPreferences(AppUtil.PREFERENCE_NAME, context.MODE_PRIVATE);
        String urlImage = sharedPreferences.getString(AppUtil.USER_IMAGE_URL,"");
        Picasso.with(context).load(Uri.parse(urlImage)).into(circleImageView);
        Button btn = (Button) v.findViewById(R.id.btn_out);
        btn.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View v) {
        SharedPreferences.Editor editor = context.getSharedPreferences(AppUtil.PREFERENCE_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(AppUtil.USER_NAME,"");
        editor.putString(AppUtil.USER_EMAIL,"");
        editor.putString(AppUtil.USER_IMAGE_URL,"");
        editor.putBoolean(AppUtil.USER_LOGIN_SESSION,false);
        editor.commit();
        LoginManager.getInstance().logOut();//cerramos la sesion de Fcbk en nuestra App
    }
}
