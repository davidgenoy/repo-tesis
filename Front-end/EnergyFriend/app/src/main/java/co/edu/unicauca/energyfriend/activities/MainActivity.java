package co.edu.unicauca.energyfriend.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.facebook.FacebookSdk;
import bolts.AppLinks;
import co.edu.unicauca.energyfriend.R;
import co.edu.unicauca.energyfriend.fragments.LoginFragment;
import co.edu.unicauca.energyfriend.fragments.DialogInviteUserFragment;
import co.edu.unicauca.energyfriend.fragments.InviteUserFragment;
import co.edu.unicauca.energyfriend.fragments.MenuFragment;
import co.edu.unicauca.energyfriend.preferences.AppUtil;


public class MainActivity extends AppCompatActivity implements LoginFragment.NavigationInterface, DialogInviteUserFragment.NavigationInterface, InviteUserFragment.NavigationInterface {

    public static final String TAG = MainActivity.class.getSimpleName();//etiqueta para mensajes en consola
    public static final String UNIVERSAL_TAG = "stack";

    LoginFragment loginFragment;
    DialogInviteUserFragment dialogInviteUserFragment;
    InviteUserFragment inviteUserFragment;
    MenuFragment menuFragment;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(UNIVERSAL_TAG,"ESTOY EN MainActivity");
        sharedPreferences = getSharedPreferences(AppUtil.PREFERENCE_NAME, MODE_PRIVATE);

        //esto es para cerrar la app si el usuario presiono el back button desde el MenuActivity
        if(getIntent().getBooleanExtra("EXIT",false)){
            finish();
        }else {

            if (sharedPreferences.getBoolean(AppUtil.USER_LOGIN_SESSION, false)) {//entra si la sesion ya esta iniciada
                Intent intent = new Intent(this,MenuActivity.class);
                //si ya hay una instancia de esta activity, limpia las q estan por encima de esta
                // y se le pasa el Intent (por medio de  onNewIntent()) a esta activity que ahora esta en el Top del Back Stack
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                //startActivity(new Intent(this, MenuActivity.class));

            } else {//entra si la sesion esta cerrada
                initAllFragments();
                putFragment(R.id.container, loginFragment);//se despliega el fragment en pantalla

                ////prueba app links///////////////////
                FacebookSdk.sdkInitialize(this);
                Uri targetUrl = AppLinks.getTargetUrlFromInboundIntent(this, getIntent());
                if (targetUrl != null) {
                    String intent = getIntent().getExtras().getString("data");
                    //Log.d("compras", "App Link Target URL: " + targetUrl.toString());
                    Log.d("compras", "App Link Target URL: " + intent);
                    Toast.makeText(this, "entre por invitacion", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void initAllFragments() {
        loginFragment = new LoginFragment();
        loginFragment.init(this,this);
        dialogInviteUserFragment = new DialogInviteUserFragment();
        dialogInviteUserFragment.init(this,this);
        inviteUserFragment = new InviteUserFragment();
        inviteUserFragment.init(this,this);
    }

    void putFragment(int idContainer, Fragment fragment) {
        FragmentTransaction fT = getSupportFragmentManager().beginTransaction();
        fT.replace(idContainer,fragment);
        fT.commit();
    }

    @Override
    public void navigationToFragment(int option) {
        switch (option){
            case -1:
                LoginFragment lf = new LoginFragment();
                lf.init(this,this);
                putFragment(R.id.container,lf);
                break;
            case 0:
                putFragment(R.id.container,dialogInviteUserFragment);
                break;
            case 1:
                putFragment(R.id.container,inviteUserFragment);
                break;
            case 2:
                /*if(menuFragment==null){
                    menuFragment = new MenuFragment(this);
                }
                putFragment(R.id.container,menuFragment);*/
                Intent intent = new Intent(this,MenuActivity.class);
                //si ya hay una instancia de esta activity, limpia las q estan por encima de esta y se le pasa el Intent (por medio de onNewIntent()) a esta activity que ahora esta en el Top del Back Stack
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                //startActivity(new Intent(this,MenuActivity.class));
                break;
        }
    }

    //escuchador cuando el usuario presiona el boton de atras

    @Override
    public void onBackPressed() {

        Log.d(UNIVERSAL_TAG,"si entro al cerrar sesion");
        /*
        Intent intent = new Intent(this,MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//si ya hay una instancia de esta activity, limpia las q estan por encima de esta y se le pasa el Intent (por medio de  onNewIntent()) a esta activity que ahora esta en el Top del Back Stack
        intent.putExtra("EXIT",true);
        startActivity(intent);*/
        finish();
        super.onBackPressed();
    }
}
