package co.edu.unicauca.energyfriend.net;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Administrador on 16/09/2016.
 */
public class FacebookManageRequest {
    public static final String TAG = FacebookManageRequest.class.getSimpleName();
    private static CallbackManager myCallbackManager;

    public interface FacebookResponseInterface{
        void onResponseFcbk(String nameUser, String emailUser, String urlImageUser);
        void onError(String error);
    }

    public static void makeFacebookRequest(CallbackManager myCallbackManager, Fragment fragment, LoginButton loginButtonFcbk, final FacebookResponseInterface facebookResponseInterface){
        loginButtonFcbk.setReadPermissions("email");
        //loginButtonFcbk.setReadPermissions("friends");
        loginButtonFcbk.setFragment(fragment);//es necesario xq estamos usando el loginButton en un Fragment
        loginButtonFcbk.registerCallback(myCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override//cuando el logeo es exitoso
                    public void onSuccess(LoginResult loginResult) {
                        AccessToken accessToken = loginResult.getAccessToken();
                        //Log.d("permisos",loginResult.getRecentlyGrantedPermissions().toString());
                        GraphRequest request =  GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {//aqui llega el json desde Facebook

                                if(response.getError()!=null){

                                }else{
                                    try {
                                        Log.d(TAG,"mi json: "+object);
                                        String name = object.getString("name");
                                        String email = object.getString("email");
                                        String urlImage = object.getJSONObject("picture").getJSONObject("data").getString("url");
                                        facebookResponseInterface.onResponseFcbk(name, email, urlImage);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        });
                        Bundle parameters = new Bundle();
                        //parameters.putString("fields", "name,email,picture{url},friends");
                        parameters.putString("fields", "name,email,picture.height(200).width(200)");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }
                    //cuando se cancela el logeo
                    @Override
                    public void onCancel() {

                        facebookResponseInterface.onError("c");
                    }
                    //cuando ocurre un error en el logeo
                    @Override
                    public void onError(FacebookException error) {
                        facebookResponseInterface.onError("e");
                    }
                });
    }
}
