package co.edu.unicauca.energyfriend.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import co.edu.unicauca.energyfriend.R;
import co.edu.unicauca.energyfriend.net.ManageRequest;
import co.edu.unicauca.energyfriend.preferences.AppUtil;

import static co.edu.unicauca.energyfriend.fragments.ChallengeFragment.POSITION_CHALLENGE_DAILY;

/**
 * A simple {@link Fragment} subclass.
 */
public class DailyFragment extends Fragment {

    //public static final int DIARIO = 0;
    LineChart lineChart;
    List<Entry> entries;
    LineDataSet dataSet;
    LineData lineData;

    //atributos para dibujar el reto (si existe)
    YAxis yAxis;
    LimitLine limitLine;
    float limitValue, promDia = 0.0f;

    ///////

    String[] etiquetas = new String[]{"01h", "02h", "03h", "04h", "05h", "06h", "07h", "08h", "09h", "10h", "11h", "12h",
            "13h", "14h", "15h", "16h", "17h", "18h", "19h", "20h", "21h", "22h", "23h", "24h"};
    Double[] varDoble = new Double[24];
    View v;

    public DailyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_daily, null);
        validar();
        return v;
    }


    public void validar() {

        ManageRequest.makeJsonRequestData(getContext(), AppUtil.URLDaily, new ManageRequest.VolleyDataResponseListener() {
            @Override
            public void onErrorDataObject(String message) {
                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponseDataObject(JSONObject response) {
                Log.d("llegadaDC","response: "+response);
                JSONArray jsonArray = null;
                try {
                    jsonArray = response.getJSONArray("out");
                    //////agregado por Stiven
                    limitValue = (float) response.getDouble("vDC");
                    /////////////////////////
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                for (int i = 0; i < etiquetas.length; i++) {

                    //JSONArray jsonArray = response.getJSONArray("out");

                    try {
                        if (jsonArray.getString(i).equals("null")) {
                            varDoble[i] = 0.0;
                            //System.out.println("Bandera "+jsonArray.getString(i));
                        } else {
                            varDoble[i] = Double.parseDouble(jsonArray.getString(i));
                        }

                    } catch (Exception e) {
                        varDoble[i] = 0.0;
                        System.out.println("catch en daily " + e);
                    }

                }
                graficar();
            }
        });

    }

    private void graficar() {
        entries = new ArrayList<>();
/**/
        for (int i = 0; i < etiquetas.length; i++) {
            entries.add(new Entry(varDoble[i].floatValue(), i));
            // System.out.println("DATOS DEL VARDOUBLE en for graficar "+varDoble[i]);
        }
        // System.out.println("Convertido A FLoat "+varDoble[1].floatValue());
        lineChart = (LineChart) v.findViewById(R.id.line_chart);

        dataSet = new LineDataSet(entries, "consumo Hora a Hora en Kwh");
        dataSet.setDrawFilled(true);//rellena el area por debajo
        //dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        lineData = new LineData(etiquetas, dataSet);
        lineChart.setData(lineData);
        lineChart.animateXY(5000, 5000);
        lineChart.setDescription("Gráfico de consumo Diario");

        if (limitValue != 0) {
            Log.d("llegadaDC", "si es diferente de cero");
            promedioDia();
            yAxis = lineChart.getAxisLeft();
            limitLine = new LimitLine(promDia, "Reto para este día: " + limitValue + " Kwh");
            limitLine.setLineWidth(2f);
            limitLine.setTextSize(12f);
            yAxis.addLimitLine(limitLine);
        }

        lineChart.invalidate();


    }

    public void promedioDia() {
        for(int k=0; k<24; k++){
            //promDia = (float) ( (promDia + varDoble[k])/24 );
            promDia = (float) ( (promDia + varDoble[k]) );
        }
        promDia = promDia/24;
        Log.d("llegadaDC","la suma del dia es: "+ promDia);
    }

}
