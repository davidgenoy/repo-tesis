package co.edu.unicauca.energyfriend.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;

import java.util.List;

/**
 * Created by Administrador on 12/12/2016.
 */
public class TabAdapter extends FragmentPagerAdapter {
    List<Fragment> data;
    String[] titlesTabs;
    int[] icons;
    Context context;

    public TabAdapter(FragmentManager fm, List<Fragment> data,  int[] icons, Context context) {
        super(fm);
        this.data = data;
        this.icons = icons;
        this.context = context;
    }

    public TabAdapter(FragmentManager fm, Context context, String[] titlesTabs, List<Fragment> data) {
        super(fm);
        this.context = context;
        this.titlesTabs = titlesTabs;
        this.data = data;
    }

    @Override
    public Fragment getItem(int position) {
        return data.get(position);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titlesTabs[position];
    }

    //este es necesario solo para la libreria
    public Drawable getIcon(int position){
        return ContextCompat.getDrawable(context, icons[position]);
    }
}
