package co.edu.unicauca.energyfriend.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import co.edu.unicauca.energyfriend.R;

/**
 * Created by Administrador on 29/09/2016.
 */

public class InviteUserFragment extends Fragment {

    //public static final String appLinkUrl = "https://fb.me/198006053966904";
    //public static final String appLinkUrl = "http://energyfriend.atwebpages.com/index.html";
    private static final String appLinkUrl = "https://fb.me/209864492781060";
    //private static final String previewImageUrl = "https://goo.gl/gkqPht";
    //private static final String previewImageUrl = "https://goo.gl/fHBgXO";
    private static final String previewImageUrl = "https://goo.gl/lGNee5";
    private static final int OPTION_MENU = 2;
    Context context;
    CallbackManager callbackManager;

    public void init(Context context, NavigationInterface navigationInterface){
        this.navigationInterface = navigationInterface;
        this.context = context;
    }

    NavigationInterface navigationInterface;

    public interface NavigationInterface{
        void navigationToFragment(int option);
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(context);
        AppEventsLogger.activateApp(getActivity().getApplication());//Registro de Evento de la aplicacion: activaciones de la aplicación
        callbackManager = CallbackManager.Factory.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_invite_user,null);

        if (AccessToken.getCurrentAccessToken() == null) {
            Toast.makeText(context,"AccessToken null",Toast.LENGTH_SHORT).show();
        } else {

            FacebookCallback<AppInviteDialog.Result> facebookCallback= new FacebookCallback<AppInviteDialog.Result>() {
                @Override
                public void onSuccess(AppInviteDialog.Result result) {
                    Toast.makeText(context,"Invitaciones enviadas correctamente",Toast.LENGTH_SHORT).show();
                    navigationInterface.navigationToFragment(OPTION_MENU);
                }

                @Override
                public void onCancel() {
                    navigationInterface.navigationToFragment(OPTION_MENU);
                }

                @Override
                public void onError(FacebookException e) {
                    Toast.makeText(context,"error al enviar invitaciones",Toast.LENGTH_SHORT).show();
                }
            };

            AppInviteDialog appInviteDialog = new AppInviteDialog(this);
            if (appInviteDialog.canShow()) {
                AppInviteContent.Builder content = new AppInviteContent.Builder();
                content.setApplinkUrl(appLinkUrl);
                content.setPreviewImageUrl(previewImageUrl);
                AppInviteContent appInviteContent = content.build();
                appInviteDialog.registerCallback(callbackManager, facebookCallback);
                appInviteDialog.show(this, appInviteContent);
            }
        }

        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //debes llamar a myCallbackManager.onActivityResult para pasar el resultado del inicio de sesión a LoginManager mediante callbackManager.
        callbackManager.onActivityResult(requestCode,resultCode,data);
    }
}
