//package co.example.administrador.energyfriend.net;
package co.edu.unicauca.energyfriend.net;

import android.content.Context;

import co.android.volley.Request;
import co.android.volley.RequestQueue;
import co.android.volley.toolbox.Volley;

/**
 * Created by Administrador on 09/09/2016.
 */
public class VolleySingletonPattern {
    private static VolleySingletonPattern volleySPInstance;
    private static RequestQueue requestQueue;
    private static Context context;

    private VolleySingletonPattern(Context context) {
        this.context = context;
        requestQueue = getRequestQueue();
    }

    public static synchronized VolleySingletonPattern getInstance(Context context){
        if(volleySPInstance == null){
            volleySPInstance = new VolleySingletonPattern(context);
        }
        return volleySPInstance;
    }

    private RequestQueue getRequestQueue(){
        if(requestQueue == null){
            requestQueue = Volley.newRequestQueue(context);
        }
        return requestQueue;
    }

    public void addToRequestQueue(Request request){
        requestQueue.add(request);
    }
}
