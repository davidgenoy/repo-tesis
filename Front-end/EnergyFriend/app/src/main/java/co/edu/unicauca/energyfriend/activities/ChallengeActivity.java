package co.edu.unicauca.energyfriend.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import co.edu.unicauca.energyfriend.R;
import co.edu.unicauca.energyfriend.adapters.TabAdapter;
import co.edu.unicauca.energyfriend.fragments.ChallengeFragment;
import co.edu.unicauca.energyfriend.net.ManageRequest;
import co.edu.unicauca.energyfriend.preferences.AppUtil;
import de.hdodenhof.circleimageview.CircleImageView;
import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;

import static co.edu.unicauca.energyfriend.activities.MenuActivity.URL_DELETE_USER;

public class ChallengeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, ViewPager.OnPageChangeListener, MaterialTabListener {

    public static final String UNIVERSAL_TAG = "stack";
    public static final String TAG = ChallengeActivity.class.getSimpleName();
    public static final String TAG_challengeFragment = "ChallengeFragment";

    CircleImageView circleImageView;
    SharedPreferences sharedPreferences;
    String userNameFcbk, urlImage, userEmail;
    TextView textViewUserName, textViewUserEmail;

    Toolbar toolbar;
    NavigationView navigationView;
    DrawerLayout drawerLayout;
    ViewPager viewPager;
    MaterialTabHost materialTabHost;

    ChallengeFragment dailyChallengeFragment, weeklyChallengeFragment, monthlyChallengeFragment;
    List<Fragment> data;
    String[] titleChallenge;
    TabAdapter tabAdapter;

    AlertDialog.Builder alertDialog;//dialogo de cerrar sesion en la opcion del navigationview

    boolean bandera0 = true, bandera1 = true, bandera2 = true;

    //variables de invitaciones
    CallbackManager callbackManagerInviteUsers;
    private static final String appLinkUrl = "https://fb.me/209864492781060";
    private static final String previewImageUrl = "https://goo.gl/lGNee5";

    SharedPreferences.Editor editor;

    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge);
        Log.d(UNIVERSAL_TAG,"ESTOY EN ChallengeActivity");
        FacebookSdk.sdkInitialize(this);
        //espacio para configurar la opcion de enviar invitaciones
        callbackManagerInviteUsers = CallbackManager.Factory.create();
        //////////////////////////////////////////////////////////
        toolbar = (Toolbar) findViewById(R.id.toolbar_challenge);
        navigationView = (NavigationView) findViewById(R.id.navigation_view_challenge);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_challenge);
        viewPager = (ViewPager) findViewById(R.id.view_pager_challenge);
        materialTabHost = (MaterialTabHost) findViewById(R.id.materialTabHostChallenge);

        titleChallenge = getResources().getStringArray(R.array.titles_challenge);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_toogle);// fijamos ícono del drawer toggle
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);//desplegamos el toogle

        View headerLayout = navigationView.getHeaderView(0);//obtenemos el layout del header del NavigationView
        textViewUserName = (TextView) headerLayout.findViewById(R.id.txt_nav_header);
        textViewUserEmail = (TextView) headerLayout.findViewById(R.id.txt_email_user);
        circleImageView = (CircleImageView) headerLayout.findViewById(R.id.profile_image);
        sharedPreferences = getSharedPreferences(AppUtil.PREFERENCE_NAME, MODE_PRIVATE);
        editor = getSharedPreferences(AppUtil.PREFERENCE_NAME, Context.MODE_PRIVATE).edit();
        urlImage = sharedPreferences.getString(AppUtil.USER_IMAGE_URL, "");
        userNameFcbk = sharedPreferences.getString(AppUtil.USER_NAME, "");
        userEmail = sharedPreferences.getString(AppUtil.USER_EMAIL, "");
        Picasso.with(this).load(Uri.parse(urlImage)).into(circleImageView);
        textViewUserName.setText(userNameFcbk);
        textViewUserEmail.setText(userEmail);

        navigationView.setNavigationItemSelectedListener(this);

        bundle = new Bundle();
        bundle.putInt(ChallengeFragment.BUNDLE_CHALLENGE,ChallengeFragment.POSITION_CHALLENGE_DAILY);//posicion 0, esto es para las tabs
        dailyChallengeFragment = new ChallengeFragment();
        dailyChallengeFragment.setArguments(bundle);//con esto, inicializamos a positionTab (ubicado en ChallengeFragment.java) en 0 necesario antes de entrar al metodo onCreateView

        bundle = new Bundle();
        bundle.putInt(ChallengeFragment.BUNDLE_CHALLENGE,ChallengeFragment.POSITION_CHALLENGE_WEEKLY);//posicion 1, esto es para las tabs
        weeklyChallengeFragment = new ChallengeFragment();
        weeklyChallengeFragment.setArguments(bundle);//con esto, inicializamos a positionTab (ubicado en ChallengeFragment.java) en 1 necesario antes de entrar al metodo onCreateView

        bundle = new Bundle();
        bundle.putInt(ChallengeFragment.BUNDLE_CHALLENGE,ChallengeFragment.POSITION_CHALLENGE_MONTHLY);//posicion 2, esto es para las tabs
        monthlyChallengeFragment = new ChallengeFragment();
        monthlyChallengeFragment.setArguments(bundle);//con esto, inicializamos a positionTab (ubicado en ChallengeFragment.java) en 2 necesario antes de entrar al metodo onCreateView

        data = new ArrayList<>();
        data.add(dailyChallengeFragment);
        data.add(weeklyChallengeFragment);
        data.add(monthlyChallengeFragment);

        tabAdapter = new TabAdapter(getSupportFragmentManager(), this, titleChallenge, data);
        viewPager.setAdapter(tabAdapter);
        viewPager.setOffscreenPageLimit(2);//con esto carga las tres pestañas al tiempo
        viewPager.addOnPageChangeListener(this);//escuchador de cambio de pagina - efecto swipe


        // Añadir 3 pestañas y asignarles un titulo de pestaña y escucha a cada pestaña
        for (int i = 0; i < tabAdapter.getCount(); i++) {
            materialTabHost.addTab(
                    materialTabHost.newTab()
                            //.setIcon(tabAdapter.getIcon(i))
                            .setText(tabAdapter.getPageTitle(i))
                            .setTabListener(this));//metodos de MaterialTabListener
        }

        /*
        -es necesario hacer la consulta para la posicion CERO de los tabs la primera vez q se lanza la UI
        -el siguiente if verifica si NO se ha establecido el reto Diario, mirando si vale -1
        -case 0 del metodo onPageSelected.
        */
        if(sharedPreferences.getFloat(AppUtil.DAILY_CHALLENGE_VALUE,-1) == -1) {
            //esta bandera es para que no se vuelva a hacer la solicitud a la BD de los 3 valores sugeridos por la app,
            // siempre y cuando el usuario no seleccione otra opcion, es decir, no se salga de las 3 pestañas
            if (bandera0) {
                Log.d(TAG_challengeFragment,"Estoy validando desde la activity!!!");
                bandera0 = false;
                dailyChallengeFragment.init(this, tabAdapter.getItem(ChallengeFragment.POSITION_CHALLENGE_DAILY));
            }
        }
        //////////////////////////////////////////////////////////////////


    }

    //escucha del NavigationView
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_historial_consumo:
                Intent intent = new Intent(this,MenuActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//si ya hay una instancia de esta activity, limpia las q estan por encima de esta y se le pasa el Intent (por medio de  onNewIntent()) a esta activity que ahora esta en el Top del Back Stack
                startActivity(intent);
                //startActivity(new Intent(this, MenuActivity.class));
                break;
            case R.id.nav_log_out:
                showAlertDialog(this);
                break;
            case R.id.nav_drop_out:
                showDropOutDialog(this);
                break;
            case R.id.nav_challenge:
                Intent intentt = new Intent(this,ChallengeActivity.class);
                intentt.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//si ya hay una instancia de esta activity, limpia las q estan por encima de esta y se le pasa el Intent (por medio de  onNewIntent()) a esta activity que ahora esta en el Top del Back Stack
                startActivity(intentt);
                //startActivity(new Intent(this, ChallengeActivity.class));
                break;
            case R.id.nav_invite_friends:
                inviteUsers();
                break;
            case R.id.nav_consumo_pesos:
                Intent intentc = new Intent(this,ConsumptionActivity.class);
                intentc.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//si ya hay una instancia de esta activity, limpia las q estan por encima de esta y se le pasa el Intent (por medio de  onNewIntent()) a esta activity que ahora esta en el Top del Back Stack
                startActivity(intentc);
                //startActivity(new Intent(this,ConsumptionActivity.class));
                break;
        }
        drawerLayout.closeDrawers();//cierra el drawer despues de seleccionar una opcion
        return true;
    }

    private void showAlertDialog(final Context context) {
        alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("¿Está seguro que desea cerrar la sesión?");
        alertDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                editor.putString(AppUtil.USER_NAME, "");
                editor.putString(AppUtil.USER_EMAIL, "");
                editor.putString(AppUtil.USER_IMAGE_URL, "");
                editor.putBoolean(AppUtil.USER_LOGIN_SESSION, false);
                editor.commit();
                LoginManager.getInstance().logOut();//cerramos la sesion de Fcbk en nuestra App
                Intent intentt = new Intent(context,MainActivity.class);
                intentt.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//si ya hay una instancia de esta activity, limpia las q estan por encima de esta y se le pasa el Intent (por medio de  onNewIntent()) a esta activity que ahora esta en el Top del Back Stack
                startActivity(intentt);
                //startActivity(new Intent(context, MainActivity.class));
            }
        });
        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }

    private void showDropOutDialog(final Context context){
        alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("¿Está seguro que desea eliminar esta cuenta definitivamente?");
        alertDialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ManageRequest.makeDropOutRequest(context, URL_DELETE_USER, new ManageRequest.VolleyDataResponseListener() {
                    @Override
                    public void onResponseDataObject(JSONObject response) {
                        try {
                            if(response.getString("out").equals("t")){
                                clearAllDataUser();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onErrorDataObject(String message) {

                    }
                });
            }
        });

        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }

    private void inviteUsers() {
        final AppInviteDialog appInviteDialog = new AppInviteDialog(this);

        if (AccessToken.getCurrentAccessToken() == null) {
            Toast.makeText(getBaseContext(), "AccessToken null", Toast.LENGTH_SHORT).show();
        } else {
            FacebookCallback<AppInviteDialog.Result> facebookCallback = new FacebookCallback<AppInviteDialog.Result>() {
                @Override
                public void onSuccess(AppInviteDialog.Result result) {
                    Toast.makeText(getBaseContext(), "Invitaciones enviadas correctamente", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onCancel() {
                    Toast.makeText(getBaseContext(), "Se ha cancelado la acción", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(FacebookException error) {
                    Toast.makeText(getBaseContext(), "Error al enviar invitaciones!", Toast.LENGTH_SHORT).show();

                }
            };

            if (appInviteDialog.canShow()) {
                AppInviteContent.Builder content = new AppInviteContent.Builder();
                content.setApplinkUrl(appLinkUrl);
                content.setPreviewImageUrl(previewImageUrl);
                AppInviteContent appInviteContent = content.build();
                appInviteDialog.registerCallback(callbackManagerInviteUsers, facebookCallback);
                appInviteDialog.show(this, appInviteContent);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManagerInviteUsers.onActivityResult(requestCode, resultCode, data);
    }


    //////////metodos de addOnPageChangeListener (efecto swipe del viewpager-cambiar de pagina)/////////////////////
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    //El método onPageSelected() se ejecuta cuando el usuario "swipea" o se desplaza a la pagina seleccionada
    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case 1:
                //el siguiente if verifica si NO se ha establecido el reto Semanal, mirando si vale -1
                if(sharedPreferences.getFloat(AppUtil.WEEKLY_CHALLENGE_VALUE,-1) == -1) {
                    //esta bandera es para que no se vuelva a hacer la solicitud a la BD de los 3 valores sugeridos por la app,
                    // siempre y cuando el usuario no seleccione otra opcion, es decir, no se salga de las 3 pestañas
                    if (bandera1) {
                        bandera1 = false;
                        weeklyChallengeFragment.init(this, tabAdapter.getItem(ChallengeFragment.POSITION_CHALLENGE_WEEKLY));
                    }
                }
                break;
            case 2:
                //el siguiente if verifica si NO se ha establecido el reto Mensual, mirando si vale -1
                if(sharedPreferences.getFloat(AppUtil.MONTHLY_CHALLENGE_VALUE,-1) == -1) {
                    //esta bandera es para que no se vuelva a hacer la solicitud a la BD de los 3 valores sugeridos por la app,
                    // siempre y cuando el usuario no seleccione otra opcion, es decir, no se salga de las 3 pestañas
                    if (bandera2) {
                        bandera2 = false;
                        monthlyChallengeFragment.init(this, tabAdapter.getItem(ChallengeFragment.POSITION_CHALLENGE_MONTHLY));
                    }
                }
                break;
        }
        materialTabHost.setSelectedNavigationItem(position);//se ordena que cambie de pestaña de acuerdo al parametro position
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    ///////fin metodos de addOnPageChangeListener////////////////////////////

    //metodos de MaterialTabListener
    @Override
    public void onTabSelected(MaterialTab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabReselected(MaterialTab tab) {

    }

    @Override
    public void onTabUnselected(MaterialTab tab) {

    }

    ////////Menu del Toolbar///////

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //drawerLayout.openDrawer(GravityCompat.START);
                drawerLayout.openDrawer(Gravity.LEFT);
                break;
        }
        return true;
    }

    //////////////fin Menu del Toolbar////////////////

    private void clearAllDataUser(){
        LoginManager.getInstance().logOut();//cerramos la sesion de Fcbk en nuestra App

        //limpiamos todas las variables de SharedPreferences
        editor.putBoolean(AppUtil.USER_LOGIN_SESSION,false);
        editor.putString(AppUtil.USER_NAME,"");
        editor.putString(AppUtil.USER_EMAIL,"");
        editor.putString(AppUtil.USER_IMAGE_URL,"");
        editor.putString(AppUtil.VALOR,"");

        editor.putInt(AppUtil.TYPE_USER, -1);
        editor.putBoolean(AppUtil.OLD_USER,false);
        editor.putBoolean(AppUtil.OPEN_DRAWER_LAYOUT, true);

        editor.putString(AppUtil.DAILY_CHALLENGE_DATE_START, "");
        editor.putString(AppUtil.DAILY_CHALLENGE_DATE_FINISH, "");
        editor.putFloat(AppUtil.DAILY_CHALLENGE_VALUE, -1);

        editor.putString(AppUtil.WEEKLY_CHALLENGE_DATE_START,"");
        editor.putString(AppUtil.WEEKLY_CHALLENGE_DATE_FINISH,"");
        editor.putFloat(AppUtil.WEEKLY_CHALLENGE_VALUE,-1);

        editor.putString(AppUtil.MONTHLY_CHALLENGE_DATE_START,"");
        //editor.putString(AppUtil.MONTHLY_CHALLENGE_DATE_FINISH,"");
        editor.putFloat(AppUtil.MONTHLY_CHALLENGE_VALUE,-1);

        editor.commit();
        Toast.makeText(this,"Usuario eliminado con éxito!",Toast.LENGTH_SHORT).show();
        //va de ultimo
        Intent intentp = new Intent(getApplicationContext(),MainActivity.class);
        intentp.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//si ya hay una instancia de esta activity, limpia las q estan por encima de esta y se le pasa el Intent (por medio de  onNewIntent()) a esta activity que ahora esta en el Top del Back Stack
        startActivity(intentp);
        //startActivity(new Intent(getApplicationContext(), MainActivity.class));
    }

    //escuchador cuando el usuario presiona el boton de atras
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this,MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//si ya hay una instancia de esta activity, limpia las q estan por encima de esta y se le pasa el Intent (por medio de  onNewIntent()) a esta activity que ahora esta en el Top del Back Stack
        intent.putExtra("EXIT",true);
        startActivity(intent);
        super.onBackPressed();
    }

}
