package co.edu.unicauca.energyfriend.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import co.edu.unicauca.energyfriend.R;
import co.edu.unicauca.energyfriend.net.ManageRequest;
import co.edu.unicauca.energyfriend.preferences.AppUtil;

/**
 * A simple {@link Fragment} subclass.
 */


public class MonthlyFragment extends Fragment {

    //Atributos
    public static final String TAG = MonthlyFragment.class.getSimpleName();

    /////***********
    BarChart barChart;
    List<BarEntry> entries;
    BarDataSet dataSet;
    BarData barData;
    //atributos para dibujar el reto (si existe)
    YAxis yAxis;
    LimitLine limitLine;
    float limitValue, promMes = 0.0f;
    /////////
    String[] etiquetas=new String[]{
            "Mes 1 ",
            "Mes 2 "
    };
    //////**********


//    CombinedChart combinedChart;
//    CombinedData data;
//    List<Entry> lineEntries;
//    List<BarEntry> barEntries;
//    LineDataSet lineDataSet;
//    LineData lineData;
//    BarData barData;
//
//    BarDataSet barDataSet;
    //String[] monthsLabels = new String[]{"Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic"};
//    String[] etiquetas=new String[]{
//            "Mes 1: ",
//            "Mes 2: ",
//
//    };
    Double[] varDoble=new  Double[24];
    View v;
    public MonthlyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_monthly, null);
        validar();

        return v;
    }

    private void validar() {
        ManageRequest.makeJsonRequestData(getContext(), AppUtil.URLMonthly, new ManageRequest.VolleyDataResponseListener() {
            @Override
            public void onErrorDataObject(String message) {
                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponseDataObject(JSONObject response) {
                Log.d(TAG,"response: "+response);
                JSONArray jsonArray = null;
                try {
                    //////agregado por Stiven
                    jsonArray = response.getJSONArray("out");
                    limitValue = (float) response.getDouble("vMC");
                    /////////////////////////
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                for (int i = 0; i < etiquetas.length; i++) {
                    //JSONArray jsonArray = response.getJSONArray("out");
                    //System.out.println("Bandera Mes "+response.length());

                    try {
                        if (jsonArray.getString(i).equals("null")) {
                            varDoble[i] = 0.0;
                            //System.out.println("Bandera "+jsonArray.getString(i));
                        } else {
                            varDoble[i] = Double.parseDouble(jsonArray.getString(i));
                        }

                    } catch (Exception e) {
                        varDoble[i] = 0.0;
                        System.out.println("catch en montlykly " + e);
                    }

                    //System.out.println("VALOR DE RESPONSE  " + response.getJSONObject("out"));

                }
                graficar();

            }
        });
    }

    private void graficar() {
        barChart = (BarChart) v.findViewById(R.id.bar_chart_monthly);
        entries = new ArrayList<>();
        for (int i=0; i<etiquetas.length;i++){
            entries.add(new BarEntry(varDoble[i].floatValue(), i));
            // System.out.println("DATOS DEL VARDOUBLE en for graficar "+varDoble[i]);
        }
        dataSet = new BarDataSet(entries,"Consumo Mensual en Kwh");
        //dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        barData = new BarData(etiquetas,dataSet);
        barChart.setData(barData);
        barChart.animateXY(3000,3000);
        barChart.setDescription("Historial de Consumo Mensual");

        if (limitValue != 0) {
            Log.d("llegadaDC", "si es diferente de cero");
            promedioMes();
            yAxis = barChart.getAxisLeft();
            limitLine = new LimitLine(promMes, "Reto para este mes: " + limitValue + " Kwh");
            limitLine.setLineWidth(2f);
            limitLine.setTextSize(12f);
            yAxis.addLimitLine(limitLine);
        }


        /*
        yAxis = barChart.getAxisLeft();
        limitLine = new LimitLine(1000.0f,"Reto para este mes: "+900.0+" Kwh");
        //limitLine.setLineColor(android.R.color.holo_red_dark);
        limitLine.setLineWidth(2f);
        //limitLine.setTextColor(android.R.color.holo_red_dark);
        limitLine.setTextSize(12f);
        yAxis.addLimitLine(limitLine);*/
        barChart.invalidate();

//        combinedChart = (CombinedChart) v.findViewById(R.id.combined_chart);
//        data = new CombinedData(etiquetas);
//
//        data.setData(getBarData());
//        data.setData(getLineData());
//
//        combinedChart.setData(data);
//        combinedChart.animateXY(3000, 3000);
    }

    // este metodo crea el grafico lineal
    /*public LineData getLineData(){
        lineEntries = new ArrayList<>();

        for (int i=0; i<etiquetas.length; i++){
            lineEntries.add(new Entry(5.7f, i));
        }

        lineDataSet = new LineDataSet(lineEntries, "Reto Energético");
        //lineDataSet.setColor(ColorTemplate.getHoloBlue());
        //lineDataSet.setColors(new int[]{ R.color.color_line_chart});
        lineData = new LineData(etiquetas,lineDataSet);
        return lineData;
    }*/

    //este metodo crea el grafico de barras
//    public BarData getBarData(){
//
//        barEntries = new ArrayList<>();
//        for (int i=0; i<etiquetas.length;i++){
//            barEntries.add(new BarEntry(varDoble[i].floatValue(), i));
//            // System.out.println("DATOS DEL VARDOUBLE en for graficar "+varDoble[i]);
//        }
//
//        /*
//        barEntries.add(new BarEntry(2.3f,0));
//        barEntries.add(new BarEntry(5.3f,1));
//        barEntries.add(new BarEntry(12.6f,2));
//        barEntries.add(new BarEntry(6.8f,3));
//        barEntries.add(new BarEntry(4.2f,4));
//        barEntries.add(new BarEntry(5.9f,5));
//        barEntries.add(new BarEntry(2.6f,6));
//        barEntries.add(new BarEntry(4.5f,7));
//        barEntries.add(new BarEntry(8.1f,8));
//        barEntries.add(new BarEntry(9.0f,9));
//        barEntries.add(new BarEntry(3.3f,10));
//        barEntries.add(new BarEntry(7.4f,11));*/
//
//        barDataSet = new BarDataSet(barEntries, "Cosumo Mes a Mes");
//        //barDataSet.setColors(new int[]{ R.color.color_line_chart});
//        barDataSet.setColor(R.color.color_line_chart);
//        barData = new BarData(etiquetas,barDataSet);
//        return barData;
//    }

    public void promedioMes() {

        for(int k=0; k<2; k++){
            //promDia = (float) ( (promDia + varDoble[k])/24 );
            promMes = (float) ( (promMes + varDoble[k]) );
        }
        promMes = promMes/2;
        Log.d("llegadaDC","la suma del mes es: "+ promMes);
    }


}
