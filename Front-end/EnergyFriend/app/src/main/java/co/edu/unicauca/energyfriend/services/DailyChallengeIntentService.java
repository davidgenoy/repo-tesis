package co.edu.unicauca.energyfriend.services;

import android.app.IntentService;
import android.app.Notification;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import co.edu.unicauca.energyfriend.R;
import co.edu.unicauca.energyfriend.fragments.ChallengeFragment;
import co.edu.unicauca.energyfriend.net.ManageRequest;
import co.edu.unicauca.energyfriend.preferences.AppUtil;


public class DailyChallengeIntentService extends IntentService {

    //Atributos
    public static final String TAG = DailyChallengeIntentService.class.getSimpleName();
    public static final int DAILY_SERVICE_POSITION = 3;
    public static final int DAILY_SERVICE_POSITION_FAILED = 9;
    public static final int DAILY_SERVICE_FLAG_POSITION = 6;
    //public static final int DAILY_SERVICE_DELAY = 40000;//este retardo (de 40 segundos)de prueba debe ser de 24 horas
    public static final int DAILY_SERVICE_DELAY = 86400000;//retardo de 24 horas
    //public static final int DAILY_SERVICE_DELAY_INVITED = 10000;//este retardo (de 10 segundos) de prueba debe ser de 15 minutos
    public static final int DAILY_SERVICE_DELAY_INVITED = 600000; //este retardo debe ser de 10 minutos
    //public static final int DAILY_SERVICE_DELAY_RESTART = 15000;//este retardo de prueba (de 15 segundos) es cuando ocurre un error y se vuelve a lanzar el service, es de 5 minutos
    public static final int DAILY_SERVICE_DELAY_RESTART = 300000;//este retardo es cuando ocurre un error y se vuelve a lanzar el service, es de 5 minutos

    public static final int ID_FOREGROUND_DAILY_SERVICE = 1;
    public static final int ID_FOREGROUND_DAILY_SERVICE_INVITED = 7;

    public static final int SUCCESSFUL_DAILY_CHALLENGE = 1;
    public static final int FAILED_DAILY_CHALLENGE = 2;
    public static final int M_OPTION_DAILY_CHALLENGE = 3;

    JSONObject jsonObject;
    float valueChallenge, vFDC;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Intent localIntent;

    public DailyChallengeIntentService() {
        super("DailyChallengeIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        sharedPreferences = getSharedPreferences(AppUtil.PREFERENCE_NAME, MODE_PRIVATE);
        editor = getSharedPreferences(AppUtil.PREFERENCE_NAME, MODE_PRIVATE).edit();
        if (intent != null) {

            String action = intent.getAction();

            if (sharedPreferences.getInt(AppUtil.TYPE_USER, -1) == 1) {//es para iniciar el service del admin de grupo

                if (AppUtil.ACTION_SERVICE_DAILY.equals(action)) {
                    String extras[];
                    extras = intent.getStringArrayExtra(AppUtil.EXTRA_SERVICE_DAILY);
                    valueChallenge = Float.parseFloat(extras[0]);
                    handleActionRun(Integer.parseInt(extras[1]));//extras[1] contiene el delay para el reto diario
                    //intent.getIntExtra(AppUtil.EXTRA_SERVICE_DAILY_DELAY,0));
                }

            } else if (sharedPreferences.getInt(AppUtil.TYPE_USER, -1) == 2) {//es para iniciar el service del invitado

                if (AppUtil.ACTION_SERVICE_DAILY_INVITED.equals(action)) {
                    int delayInvited = intent.getIntExtra(AppUtil.EXTRA_SERVICE_DAILY_INVITED, 0);
                    handleInvitedActionRun(delayInvited);
                }

            }

        }
    }

    private void handleActionRun(int delayTime) {

        //construimos la notificacion en el StatusBar
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Reto Diario establecido!")
                .setContentText("esperando...")
                .setPriority(Notification.PRIORITY_MIN);

        startForeground(ID_FOREGROUND_DAILY_SERVICE, notificationBuilder.build());//establecer el service en primer plano
        try {
            Thread.sleep(delayTime); // dormir el hilo 24 horas sino entra a traves del catch, de lo contrario es de 5 minutos.
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //una vez cumplidas las 24 horas (o los 5 minutos):
        try {
            jsonObject = ManageRequest.makeValueFinishXChallengeRequest(getBaseContext(), ChallengeFragment.URL, DAILY_SERVICE_POSITION);

            try {
                Log.d(TAG, "llego: " + jsonObject);
                if (jsonObject != null) {
                    vFDC = (float) jsonObject.getDouble("vFDC");
                    if ((valueChallenge - vFDC) >= 0) {//si entra, cumplio con el reto de tipo Diario
                        localIntent = new Intent(AppUtil.ACTION_SERVICE_DAILY).putExtra(AppUtil.EXTRA_SERVICE_DAILY, SUCCESSFUL_DAILY_CHALLENGE);
                        ManageRequest.insertFlagSuccessXChallengeRequest(getBaseContext(), ChallengeFragment.URL_FLAG, DAILY_SERVICE_POSITION);//establecer a 'y' en el campo 'banderaRD' de la tabla RETO
                    } else {//si entra, NO SE cumplio con el reto de tipo Diario
                        localIntent = new Intent(AppUtil.ACTION_SERVICE_DAILY).putExtra(AppUtil.EXTRA_SERVICE_DAILY, FAILED_DAILY_CHALLENGE);
                        ManageRequest.insertFlagFailedXChallengeRequest(getBaseContext(), ChallengeFragment.URL_FLAG, DAILY_SERVICE_POSITION_FAILED);//establecer a 'n' en el campo 'banderaRD' de la tabla RETO
                    }
                    //Thread.sleep(30000);//retardo de prueba (es de 30 seg), debe ser de media hora
                    Thread.sleep(1800000);//retardo de media hora para darle tiempo a los usuarios de tipo 2 (invitado) a que puedan recibir la notificacion de reto cumplido

                    LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(localIntent);// Emisión de localIntent
                    Thread.sleep(300);//este retado parece ser que es necesario para que se reciba correctamente el dato antes de llamar a stopForeground().

                    //una vez se cumpla el tiempo, eliminamos la fecha de inicio, de fin y el valor del reto Diario, para poder volver a establecerlos en el ChallengeFragment
                    editor.putString(AppUtil.DAILY_CHALLENGE_DATE_START, "");
                    editor.putString(AppUtil.DAILY_CHALLENGE_DATE_FINISH, "");
                    editor.putFloat(AppUtil.DAILY_CHALLENGE_VALUE, -1);
                    editor.commit();

                    //una vez se cumpla el tiempo, eliminamos la fecha de inicio, de fin, el valor del reto Diario y establecemos banderaRD en 'm' en el servidor
                    ManageRequest.makeClearChallengeRequest(getBaseContext(), ChallengeFragment.URL_INSERT_CHALLENGE, DAILY_SERVICE_POSITION);

                    stopForeground(true); //quitamos el service del primer plano
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    //service para el Usuario de tipo Invitado
    private void handleInvitedActionRun(int delayInvited) {
        Log.d(TAG, "estoy en el service Diario del invitado");
        //construimos la notificacion en el StatusBar
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Esperando por Reto Diario!")
                .setContentText("esperando...")
                .setPriority(Notification.PRIORITY_MIN);

        startForeground(ID_FOREGROUND_DAILY_SERVICE_INVITED, notificationBuilder.build());//establecer el service en primer plano

        try {
            Thread.sleep(delayInvited); //retardo de 10 minutos sino entra a traves del catch, sino es de 5 min.
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //una vez cumplidos los 10 minutos (o 5 minutos):
        jsonObject = ManageRequest.makeFlagChallengeRequest(getBaseContext(), ChallengeFragment.URL_FLAG, DAILY_SERVICE_FLAG_POSITION);
        Log.d(TAG, "invitado recibio el sgte JsonObject: " + jsonObject);
        if (jsonObject != null) {
            try {
                String flag;
                flag = jsonObject.getString("flagRD");
//*********************************************
                if (flag.equals("m")) {
                    localIntent = new Intent(AppUtil.ACTION_SERVICE_DAILY_INVITED).putExtra(AppUtil.EXTRA_SERVICE_DAILY_INVITED, M_OPTION_DAILY_CHALLENGE);
                    editor.putBoolean(AppUtil.SERV_DAILY_INVITED_HAS_NOT_BEEN_SEEN, true);
                } else {
                    if (sharedPreferences.getBoolean(AppUtil.SERV_DAILY_INVITED_HAS_NOT_BEEN_SEEN, true)) {//esta validacion es para que solo verifique una vez si cumplio con el reto y no se vuelva a lanzar la notificacion
                        Log.d(TAG, "entro a validar si es: y o n");
                        editor.putBoolean(AppUtil.SERV_DAILY_INVITED_HAS_NOT_BEEN_SEEN, false);
                        if (flag.equals("y")) {
                            localIntent = new Intent(AppUtil.ACTION_SERVICE_DAILY_INVITED).putExtra(AppUtil.EXTRA_SERVICE_DAILY_INVITED, SUCCESSFUL_DAILY_CHALLENGE);
                        } else if (flag.equals("n")) {
                            localIntent = new Intent(AppUtil.ACTION_SERVICE_DAILY_INVITED).putExtra(AppUtil.EXTRA_SERVICE_DAILY_INVITED, FAILED_DAILY_CHALLENGE);
                        }
                    }
                    stopSelf();//detenemos el servicio
                }
                editor.commit();
//*********************************************
                /*if(flag.equals("y")){
                    localIntent = new Intent(AppUtil.ACTION_SERVICE_DAILY_INVITED).putExtra(AppUtil.EXTRA_SERVICE_DAILY_INVITED, SUCCESSFUL_DAILY_CHALLENGE);
                }else if(flag.equals("n")){
                    localIntent = new Intent(AppUtil.ACTION_SERVICE_DAILY_INVITED).putExtra(AppUtil.EXTRA_SERVICE_DAILY_INVITED, FAILED_DAILY_CHALLENGE);
                }else if(flag.equals("m")){
                    localIntent = new Intent(AppUtil.ACTION_SERVICE_DAILY_INVITED).putExtra(AppUtil.EXTRA_SERVICE_DAILY_INVITED, M_OPTION_DAILY_CHALLENGE);
                }*/
                LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(localIntent);// Emisión de  localIntent
                try {
                    Thread.sleep(300);//este retado parece ser que es necesario para que se reciba correctamente el dato antes de llamar a stopForeground().
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                stopForeground(true);//quitamos el service del primer plano
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "Servicio destruido...");
    }

}
