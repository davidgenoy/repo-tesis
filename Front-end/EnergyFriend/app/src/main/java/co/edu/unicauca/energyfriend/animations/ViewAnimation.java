package co.edu.unicauca.energyfriend.animations;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import com.facebook.login.widget.LoginButton;
import co.edu.unicauca.energyfriend.R;

/**
 * Created by Administrador on 20/11/2016.
 */
public class ViewAnimation {
    static LoginButton loginButton;
    static TextView txt_msg_fcbk;
    static Animation movimiento[] = new Animation[2];

    public static void setAnimationBtnContinue(final Context context, final Button btnContinue, LoginButton loginBtn, TextView txt_fcbk){
        loginButton = loginBtn;
        txt_msg_fcbk = txt_fcbk;
        //Animation movimiento;
        movimiento[0] = AnimationUtils.loadAnimation(context, R.animator.mover);
        movimiento[0].reset();
        btnContinue.startAnimation(movimiento[0]);

        movimiento[0].setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                btnContinue.setVisibility(View.GONE);//ya no es visible el boton de continue en pantalla
                txt_msg_fcbk.setVisibility(View.VISIBLE);
                loginButton.setVisibility(View.VISIBLE);
                movimiento[1] = AnimationUtils.loadAnimation(context, R.animator.mover_btn_fcbk);
                movimiento[1].reset();
                txt_msg_fcbk.startAnimation(movimiento[1]);
                loginButton.startAnimation(movimiento[1]);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

}
