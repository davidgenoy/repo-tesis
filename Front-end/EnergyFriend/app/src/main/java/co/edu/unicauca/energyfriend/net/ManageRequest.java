//package co.example.administrador.energyfriend.net;
package co.edu.unicauca.energyfriend.net;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import co.android.volley.NoConnectionError;
import co.android.volley.Request;
import co.android.volley.Response;
import co.android.volley.VolleyError;
import co.android.volley.toolbox.JsonObjectRequest;
import co.android.volley.toolbox.RequestFuture;
import co.edu.unicauca.energyfriend.activities.MenuActivity;
import co.edu.unicauca.energyfriend.fragments.ChallengeFragment;
import co.edu.unicauca.energyfriend.fragments.LoginFragment;
import co.edu.unicauca.energyfriend.preferences.AppUtil;
import co.edu.unicauca.energyfriend.services.DailyChallengeIntentService;
import co.edu.unicauca.energyfriend.services.MonthlyChallengeIntentService;
import co.edu.unicauca.energyfriend.services.WeeklyChallengeIntentService;

//import co.example.administrador.energyfriend.fragments.LoginFragment;

/**
 * Created by Administrador on 10/09/2016.
 */

public class ManageRequest {

    //atributos
    public static final String TAG = ManageRequest.class.getSimpleName(); //etiqueta para mensajes de consola
    static SharedPreferences sharedPreferences;


    public interface VolleyDataResponseListener {
        void onResponseDataObject(JSONObject response);
        void onErrorDataObject(String message);
    }

    public interface VolleyChallengeDataListener {
        void onResponseChallengeData(JSONObject response);
        void onErrorChallengeData(String message);
    }

    public static void makeJsonRequestData(Context context, String url, final VolleyDataResponseListener listener) {
        HashMap<String, String> params = new HashMap<>();
        sharedPreferences = context.getSharedPreferences(AppUtil.PREFERENCE_NAME, context.MODE_PRIVATE);
        String codigo = sharedPreferences.getString(AppUtil.VALOR, "");
        params.put("codigo", codigo);
        // System.out.println("Numero del contador en ManageRequest"+codigo);
        JSONObject jsonObject = new JSONObject(params);
        Log.d("MonthlyFragment","salio: "+jsonObject);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                url,
                jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //String rta = response.getString("out");
                        //    System.out.println("llego: " + response);
                        listener.onResponseDataObject(response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                 /*StringWriter errors = new StringWriter();
                        error.printStackTrace(new PrintWriter(errors));
                        Log.d("llegada",errors.toString());*/

                if (error instanceof NoConnectionError) {
                    listener.onErrorDataObject("No hay conexion a internet!");
                }

            }
        }
        );

        VolleySingletonPattern.getInstance(context).addToRequestQueue(jsonObjectRequest);

    }

    public interface VolleyResponseListener {
        void onError(String message);
        void onResponse(JSONObject response);
    }

    public static void makeJsonRequest(Context context, int codigoContador, String url, final VolleyResponseListener listener) {
        HashMap<String, Integer> params = new HashMap<>();
        params.put(LoginFragment.KEY_CODIGO, codigoContador);
        JSONObject jsonObject = new JSONObject(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                url,
                jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {//llega el json con la rta del servidor
                        Log.d("mirta", response.toString());
                        listener.onResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*StringWriter errors = new StringWriter();
                        error.printStackTrace(new PrintWriter(errors));
                        Log.d("llegada",errors.toString());*/

                        if (error instanceof NoConnectionError) {
                            listener.onError("No hay conexion a internet!");
                        }
                    }
                });
        VolleySingletonPattern.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public static void validateInsertUser(Context context, String codigoContador, String emailUser, String url, final VolleyResponseListener listener) {
        HashMap<String, String> params = new HashMap<>();
        params.put(LoginFragment.KEY_CODIGO, codigoContador);
        params.put(LoginFragment.KEY_EMAIL, emailUser);

        JSONObject jsonObject = new JSONObject(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                url,
                jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        listener.onResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof NoConnectionError) {
                            listener.onError("No hay conexion a internet!");
                        }
                    }
                });
        VolleySingletonPattern.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public static void makeChallengeRequest(Context context, String url, int position, final VolleyChallengeDataListener listener) {
        sharedPreferences = context.getSharedPreferences(AppUtil.PREFERENCE_NAME, context.MODE_PRIVATE);
        String codigo = sharedPreferences.getString(AppUtil.VALOR, "");
        HashMap<String, String> params = new HashMap<>();
        params.put(ChallengeFragment.CHALLENGE, position + "");
        params.put(ChallengeFragment.CODIGO_CONTADOR, codigo);

        JSONObject jsonObject = new JSONObject(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                url,
                jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) throws JSONException {
                        listener.onResponseChallengeData(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof NoConnectionError) {
                            listener.onErrorChallengeData("No hay conexion a internet!");
                        }
                    }
                }
        );
        VolleySingletonPattern.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public static void makeInsertChallengeRequest(Context context, String url, int position, String fechaCreacion, String valorReto, final VolleyChallengeDataListener listener) {
        sharedPreferences = context.getSharedPreferences(AppUtil.PREFERENCE_NAME, context.MODE_PRIVATE);
        String codigo = sharedPreferences.getString(AppUtil.VALOR, "");
        HashMap<String, String> params = new HashMap<>();
        params.put(ChallengeFragment.CODIGO_CONTADOR, codigo);
        params.put(ChallengeFragment.CHALLENGE, position + "");
        params.put(ChallengeFragment.DATE_CREATE_CHALLENGE, fechaCreacion);
        params.put(ChallengeFragment.VALUE_CHALLENGE, valorReto);

        JSONObject jsonObject = new JSONObject(params);
        Log.d(TAG,"salio: "+jsonObject);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                url,
                jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) throws JSONException {
                        Log.d(TAG,"llego: "+response);
                        listener.onResponseChallengeData(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof NoConnectionError) {
                            listener.onErrorChallengeData("No hay conexion a internet!");
                            Log.d("llegadaJson","llego: ERROR");
                        }
                    }
                }
        );
        VolleySingletonPattern.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public static void makeInsertWeeklyChallengeRequest(Context context, String url, String fechaCreacion, String valorReto, final VolleyChallengeDataListener listener) {
        sharedPreferences = context.getSharedPreferences(AppUtil.PREFERENCE_NAME, context.MODE_PRIVATE);
        String codigo = sharedPreferences.getString(AppUtil.VALOR, "");
        HashMap<String, String> params = new HashMap<>();
        params.put(ChallengeFragment.CODIGO_CONTADOR, codigo);
        params.put(ChallengeFragment.DATE_CREATE_CHALLENGE, fechaCreacion);
        params.put(ChallengeFragment.VALUE_CHALLENGE, valorReto);

        JSONObject jsonObject = new JSONObject(params);
        Log.d("ChallengeFragment","salio: "+jsonObject);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                url,
                jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) throws JSONException {
                        Log.d("ChallengeFragment","llego: "+response);
                        listener.onResponseChallengeData(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof NoConnectionError) {
                            listener.onErrorChallengeData("No hay conexion a internet!");

                        }
                    }
                }
        );
        VolleySingletonPattern.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }



    public static void makeClearChallengeRequest(Context context, String url, int position) {
        sharedPreferences = context.getSharedPreferences(AppUtil.PREFERENCE_NAME, context.MODE_PRIVATE);
        String codigo = sharedPreferences.getString(AppUtil.VALOR, "");
        HashMap<String, String> params = new HashMap<>();
        params.put(ChallengeFragment.CODIGO_CONTADOR, codigo);
        params.put(ChallengeFragment.CHALLENGE, position + "");

        JSONObject jsonObject = new JSONObject(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                url,
                jsonObject,
                null,
                null
        );
        VolleySingletonPattern.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public static void insertFlagSuccessXChallengeRequest(Context context, String url, int position) {
        sharedPreferences = context.getSharedPreferences(AppUtil.PREFERENCE_NAME, context.MODE_PRIVATE);
        String codigo = sharedPreferences.getString(AppUtil.VALOR, "");
        HashMap<String, String> params = new HashMap<>();
        params.put(ChallengeFragment.CODIGO_CONTADOR, codigo);
        params.put(ChallengeFragment.CHALLENGE, position + "");

        JSONObject jsonObject = new JSONObject(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                url,
                jsonObject,
                null,
                null
        );
        VolleySingletonPattern.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public static void insertFlagFailedXChallengeRequest(Context context, String url, int position) {
        sharedPreferences = context.getSharedPreferences(AppUtil.PREFERENCE_NAME, context.MODE_PRIVATE);
        String codigo = sharedPreferences.getString(AppUtil.VALOR, "");
        HashMap<String, String> params = new HashMap<>();
        params.put(ChallengeFragment.CODIGO_CONTADOR, codigo);
        params.put(ChallengeFragment.CHALLENGE, position + "");

        JSONObject jsonObject = new JSONObject(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                url,
                jsonObject,
                null,
                null
        );
        VolleySingletonPattern.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }


    public static void makeTypeUserRequest(Context context, String url, final VolleyDataResponseListener listener){
        sharedPreferences = context.getSharedPreferences(AppUtil.PREFERENCE_NAME, context.MODE_PRIVATE);
        String userEmail = sharedPreferences.getString(AppUtil.USER_EMAIL, "");
        HashMap<String, String> params = new HashMap<>();
        params.put(LoginFragment.KEY_EMAIL, userEmail);
        JSONObject jsonObject = new JSONObject(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                url,
                jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) throws JSONException {
                        listener.onResponseDataObject(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof NoConnectionError) {
                            listener.onErrorDataObject("No hay conexion a internet!");
                        }
                    }
                }
        );
        VolleySingletonPattern.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public static void makeKwhRequest(Context context, String url, final VolleyDataResponseListener listener){
        sharedPreferences = context.getSharedPreferences(AppUtil.PREFERENCE_NAME, context.MODE_PRIVATE);
        String codigo = sharedPreferences.getString(AppUtil.VALOR, "");
        HashMap<String, String> params = new HashMap<>();
        params.put(ChallengeFragment.CODIGO_CONTADOR, codigo);

        JSONObject jsonObject = new JSONObject(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                url,
                jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) throws JSONException {
                        listener.onResponseDataObject(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof NoConnectionError) {
                            listener.onErrorDataObject("No hay conexion a internet!");
                        }
                    }
                }
        );
        VolleySingletonPattern.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public static void makeDropOutRequest(Context context, String url, final VolleyDataResponseListener listener){
        sharedPreferences = context.getSharedPreferences(AppUtil.PREFERENCE_NAME, context.MODE_PRIVATE);
        String codigo = sharedPreferences.getString(AppUtil.VALOR, "");
        String userEmail = sharedPreferences.getString(AppUtil.USER_EMAIL, "");
        HashMap<String, String> params = new HashMap<>();
        params.put(ChallengeFragment.CODIGO_CONTADOR, codigo);
        params.put(LoginFragment.KEY_EMAIL, userEmail);

        JSONObject jsonObject = new JSONObject(params);
        Log.d("makeDropOutRequest","envio: "+jsonObject);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                url,
                jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) throws JSONException {
                        Log.d("makeDropOutRequest","llego: "+response);
                        listener.onResponseDataObject(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof NoConnectionError) {
                            listener.onErrorDataObject("No hay conexion a internet!");
                        }
                    }
                }
        );
        VolleySingletonPattern.getInstance(context).addToRequestQueue(jsonObjectRequest);
    }

    public static JSONObject makeValueFinishXChallengeRequest(Context context, String url, int position) {
        String codigo = sharedPreferences.getString(AppUtil.VALOR, "");
        HashMap<String,String> paramsJson = new HashMap<>();
        paramsJson.put(ChallengeFragment.CHALLENGE, position+""); //position = 3,4,5->llegan desde el service (D,W,M)
        paramsJson.put(ChallengeFragment.CODIGO_CONTADOR, codigo);
        JSONObject jsonObject = new JSONObject(paramsJson);

        RequestFuture<JSONObject> requestFuture = RequestFuture.newFuture();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                url,
                jsonObject,
                requestFuture,
                requestFuture
        );
        VolleySingletonPattern.getInstance(context).addToRequestQueue(jsonObjectRequest);
        try {
            return requestFuture.get(10, TimeUnit.SECONDS); //retornamos la request, esperando máximo 10 segundos.
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            //entra aqui cuando no se pudo conectar al servidor, vuelve a re lanzar el service y se ejecuta hasta que se pueda conectar con el servidor
            e.printStackTrace();
            Log.d(TAG, "entro a executionException!!!");
            Intent intentChallenge;
            switch (position){
                case 3: //Daily Service
                    intentChallenge = new Intent(context, DailyChallengeIntentService.class);
                    intentChallenge.setAction(AppUtil.ACTION_SERVICE_DAILY);
                    intentChallenge.putExtra(AppUtil.EXTRA_SERVICE_DAILY, new String[]{
                            String.valueOf(sharedPreferences.getFloat(AppUtil.DAILY_CHALLENGE_VALUE,-1)),
                            String.valueOf(DailyChallengeIntentService.DAILY_SERVICE_DELAY_RESTART)
                    });
                    context.startService(intentChallenge);
                    break;
                case 4: //Weekly Service
                    intentChallenge = new Intent(context, WeeklyChallengeIntentService.class);
                    intentChallenge.setAction(AppUtil.ACTION_SERVICE_WEEKLY);
                    intentChallenge.putExtra(AppUtil.EXTRA_SERVICE_WEEKLY, new String[]{
                            String.valueOf(sharedPreferences.getFloat(AppUtil.WEEKLY_CHALLENGE_VALUE,-1)),
                            String.valueOf(WeeklyChallengeIntentService.WEEKLY_SERVICE_DELAY_RESTART)
                    });
                    context.startService(intentChallenge);
                    break;
                case 5: //Monthly Service
                    intentChallenge = new Intent(context, MonthlyChallengeIntentService.class);
                    intentChallenge.setAction(AppUtil.ACTION_SERVICE_MONTHLY);
                    intentChallenge.putExtra(AppUtil.EXTRA_SERVICE_MONTHLY, new String[]{
                            String.valueOf(sharedPreferences.getFloat(AppUtil.MONTHLY_CHALLENGE_VALUE,-1)),
                            String.valueOf(MonthlyChallengeIntentService.MONTHLY_SERVICE_DELAY_RESTART)
                    });
                    context.startService(intentChallenge);
                    break;
            }

        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static JSONObject makeFlagChallengeRequest(Context context,String url, int position) {
        String codigo = sharedPreferences.getString(AppUtil.VALOR, "");
        HashMap<String,String> paramsJson = new HashMap<>();
        paramsJson.put(ChallengeFragment.CHALLENGE, position+""); //position = 3,4,5->llegan desde el service (D,W,M)
        paramsJson.put(ChallengeFragment.CODIGO_CONTADOR, codigo);
        JSONObject jsonObject = new JSONObject(paramsJson);

        RequestFuture<JSONObject> requestFuture = RequestFuture.newFuture();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                url,
                jsonObject,
                requestFuture,
                requestFuture
        );
        VolleySingletonPattern.getInstance(context).addToRequestQueue(jsonObjectRequest);
        try {
            return requestFuture.get(10, TimeUnit.SECONDS); //retornamos la request, esperando máximo 10 segundos.
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            //entra aqui cuando no se pudo conectar al servidor, vuelve a re lanzar el service y se ejecuta hasta que se pueda conectar con el servidor
            e.printStackTrace();
            Log.d(TAG, "entro a executioneception!!!");
            Intent intentChallenge;
            switch (position){
                case 3: //Daily Service
                    intentChallenge = new Intent(context, DailyChallengeIntentService.class);
                    intentChallenge.setAction(AppUtil.ACTION_SERVICE_DAILY_INVITED);
                    intentChallenge.putExtra(AppUtil.EXTRA_SERVICE_DAILY_INVITED, DailyChallengeIntentService.DAILY_SERVICE_DELAY_RESTART);
                    context.startService(intentChallenge);
                    break;
                case 4: //Weekly Service
                    intentChallenge = new Intent(context, WeeklyChallengeIntentService.class);
                    intentChallenge.setAction(AppUtil.ACTION_SERVICE_WEEKLY_INVITED);
                    intentChallenge.putExtra(AppUtil.EXTRA_SERVICE_WEEKLY_INVITED, WeeklyChallengeIntentService.WEEKLY_SERVICE_DELAY_RESTART);
                    context.startService(intentChallenge);
                    break;
                case 5: //Monthly Service
                    intentChallenge = new Intent(context, MonthlyChallengeIntentService.class);
                    intentChallenge.setAction(AppUtil.ACTION_SERVICE_MONTHLY_INVITED);
                    intentChallenge.putExtra(AppUtil.EXTRA_SERVICE_MONTHLY_INVITED, MonthlyChallengeIntentService.MONTHLY_SERVICE_DELAY_RESTART);
                    context.startService(intentChallenge);
                    break;
            }

        } catch (TimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }


}

