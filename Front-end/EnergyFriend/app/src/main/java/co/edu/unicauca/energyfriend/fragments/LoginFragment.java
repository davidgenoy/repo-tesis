package co.edu.unicauca.energyfriend.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import co.edu.unicauca.energyfriend.R;
import co.edu.unicauca.energyfriend.animations.ViewAnimation;
import co.edu.unicauca.energyfriend.net.FacebookManageRequest;
import co.edu.unicauca.energyfriend.net.ManageRequest;
import co.edu.unicauca.energyfriend.preferences.AppUtil;


public class LoginFragment extends Fragment implements View.OnClickListener, TextWatcher {
    //final static String url = "http://192.168.0.29/testVolley/requestVolley.php";
    //final static String url = "http://energyfriend.comlu.com/managmentRequest.php";
    //final static String url = "http://energyfriend.byethost7.com/managmentRequest.php";
    final static String url = "http://energyfriend.atwebpages.com/managmentRequest.php";
    final static String urlUser = "http://energyfriend.atwebpages.com/validate.php";
    //final static String url = "http://192.168.0.27/tesis/managmentRequest.php";
    //final static String urlUser = "http://192.168.0.27/tesis/validate.php";
    public static final String KEY_CODIGO = "codigo";
    public static final String KEY_EMAIL = "email";
    public static final int OPTION = 0;
    public static final int OPTION_ACCESS_DENIED = -1;
    public static final String  fontPath= "encinocaps/Encino Caps.ttf";

    String til_codigo_string;
    Context context;
    Button btn_continue;
    LoginButton loginButtonFcbk;
    TextInputLayout til_login_codigo_contador;
    EditText edt_codigo_contador;
    TextView txt_msg_fcbk,txt_title;
    CallbackManager myCallbackManager;

    NavigationInterface navigationInterface;

    public interface NavigationInterface{
        void navigationToFragment(int option);
    }

    public LoginFragment() {
    }



    public void init(Context context, NavigationInterface navigationInterface){
        this.navigationInterface = navigationInterface;
        this.context = context;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(context);
        AppEventsLogger.activateApp(getActivity().getApplication());//Registro de Evento de la aplicacion: activaciones de la aplicación
        myCallbackManager = CallbackManager.Factory.create();//inicializa la instancia de CallbackManager
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login,null);
        //Typeface typeface = Typeface.createFromAsset(context.getAssets(),fontPath);//esto es para el tipo de letra
        //txt_title = (TextView) v.findViewById(R.id.txt_title);
        btn_continue = (Button) v.findViewById(R.id.btn_login_continue);
        //txt_title.setTypeface(typeface);//fijamos tipo de letra al View
        //btn_continue.setTypeface(typeface);//fijamos tipo de letra al View

        til_login_codigo_contador = (TextInputLayout) v.findViewById(R.id.til_login_codigo_contador);
        edt_codigo_contador = (EditText) v.findViewById(R.id.edt_codigo_contador);
        edt_codigo_contador.getBackground().setColorFilter(ContextCompat.getColor(context,R.color.color_line_edt), PorterDuff.Mode.SRC_IN);
        txt_msg_fcbk = (TextView) v.findViewById(R.id.txt_msg_fcbk);
        edt_codigo_contador.addTextChangedListener(this);//escuchador de evento de cambio de texto en el EditText
        btn_continue.setOnClickListener(this);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loginButtonFcbk = (LoginButton) view.findViewById(R.id.btn_login_fcbk);
        loginButtonFcbk.setOnClickListener(this);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //debes llamar a myCallbackManager.onActivityResult para pasar el resultado del inicio de sesión a LoginManager mediante callbackManager.
        myCallbackManager.onActivityResult(requestCode,resultCode,data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_login_continue:
                validateCounterCode();
                break;
            case R.id.btn_login_fcbk:
                validateCounterFacebook();
                break;
        }
    }

    private void validateCounterCode() {
        til_codigo_string = til_login_codigo_contador.getEditText().getText().toString();
        //if(!Patterns.PHONE.matcher(til_codigo).matches() || til_codigo.length() != 8 )
        if(til_codigo_string.length() != 8 )
            til_login_codigo_contador.setError("codigo no válido");
        else{
            til_login_codigo_contador.setError(null);
            sendRequest(Integer.parseInt(til_codigo_string));//consulta si existe el codigo ingresado en la BD
        }
    }

    private void sendRequest(int codigo) {

        ManageRequest.makeJsonRequest(context, codigo, url, new ManageRequest.VolleyResponseListener() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    if(response.getString("rta").equals("t")){
                        til_login_codigo_contador.setEnabled(false);//inhabilita el textInputLayout pero se muestra en pantalla
                        ViewAnimation.setAnimationBtnContinue(context, btn_continue, loginButtonFcbk, txt_msg_fcbk);
                    }else if(response.getString("rta").equals("f")) {
                        Toast.makeText(context, "el codigo no existe!", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(String message) {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void validateCounterFacebook() {
        FacebookManageRequest.makeFacebookRequest(myCallbackManager, this, loginButtonFcbk, new FacebookManageRequest.FacebookResponseInterface() {
            @Override
            public void onResponseFcbk(final String nameUser, final String emailUser, final String urlImageUser) {
                loginButtonFcbk.setEnabled(false);//inhabilitamos el boton de Fcbk
                //guardamos el correo del usuario en la BD
                ManageRequest.validateInsertUser(context, til_codigo_string, emailUser, urlUser, new ManageRequest.VolleyResponseListener(){
                    //llega la respuesta del servidor
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getString("rta").equals("t")){
                                //guardamos la sesion del usuario
                                SharedPreferences.Editor editor = context.getSharedPreferences(AppUtil.PREFERENCE_NAME, Context.MODE_PRIVATE).edit();
                                editor.putString(AppUtil.USER_NAME,nameUser);
                                editor.putString(AppUtil.USER_EMAIL,emailUser);
                                editor.putString(AppUtil.USER_IMAGE_URL,urlImageUser);
                                editor.putString(AppUtil.VALOR,til_codigo_string);
                                editor.putBoolean(AppUtil.USER_LOGIN_SESSION,true);
                                editor.commit();

                                navigationInterface.navigationToFragment(OPTION);
                            }else if(response.getString("rta").equals("aoc")){
                                Toast.makeText(context, "el usuario ya está asociado a otro contador!",Toast.LENGTH_LONG).show();
                                //startActivity(new Intent(context, MainActivity.class));
                                navigationInterface.navigationToFragment(OPTION_ACCESS_DENIED);
                                LoginManager.getInstance().logOut();//cerramos la sesion de Fcbk en nuestra App
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(String message) {
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onError(String error) {
                Toast.makeText(context,"hubo un error o cancelacion",Toast.LENGTH_SHORT).show();
            }
        });
    }


    /////////Los tres metodos siguientes son escuchadores de evento de cambio de texto en el EditText en tiempo real
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        til_login_codigo_contador.setError(null);
    }
    @Override
    public void afterTextChanged(Editable s) {
    }
    /////////////////////////////////////////////////////////////
}
