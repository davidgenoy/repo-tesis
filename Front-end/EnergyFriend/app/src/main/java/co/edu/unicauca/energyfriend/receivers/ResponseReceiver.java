package co.edu.unicauca.energyfriend.receivers;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import co.edu.unicauca.energyfriend.R;
import co.edu.unicauca.energyfriend.activities.MenuActivity;
import co.edu.unicauca.energyfriend.activities.WinnerActivity;
import co.edu.unicauca.energyfriend.preferences.AppUtil;
import co.edu.unicauca.energyfriend.services.DailyChallengeIntentService;
import co.edu.unicauca.energyfriend.services.MonthlyChallengeIntentService;
import co.edu.unicauca.energyfriend.services.WeeklyChallengeIntentService;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Administrador on 18/01/2017.
 */

public class ResponseReceiver extends BroadcastReceiver {

    //Atributos
    Context myContext;
    Intent myIntent;
    PendingIntent pendingIntent;

    public static final int CLEAR_WEEKLY_CHALLENGE = 4;
    public static final int CLEAR_MONTHLY_CHALLENGE = 5;
    Intent intentChallenge;
    SharedPreferences sharedPreferences;

    public ResponseReceiver(Context context) {
        myContext = context;
        sharedPreferences = myContext.getSharedPreferences(AppUtil.PREFERENCE_NAME, MODE_PRIVATE);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        int rta;
        //El primer índice del array es el número de milisegundos que debe esperar antes de activar el vibrador
        //y el segundo índice es el número de milisegundos para vibrar antes de apagarse.
        long[] vibrationTime = {500, 1000};
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);//para el sonido


        switch (intent.getAction()) {

            case AppUtil.ACTION_SERVICE_DAILY:

                rta = intent.getIntExtra(AppUtil.EXTRA_SERVICE_DAILY, -1);

                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(myContext)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setVibrate(vibrationTime)
                        .setSound(uri)
                        .setAutoCancel(true); //para cerrar la notificacion una vez de presione sobre ella.

                NotificationManager notificationManager;//NotificationManager muestra la notificacion al usuario usando el metodo notify.

                if (rta == 1) {
                    myIntent = new Intent(myContext, WinnerActivity.class);//.putExtra(WinnerActivity.WINNER_EXTRA,WinnerActivity.WINNER_DAILY);
                    notificationBuilder.setContentTitle("Reto Diario cumplido!").setContentText("Felicidades...pulsa aquí!!!");
                } else if (rta == 2) {
                    myIntent = new Intent(myContext, MenuActivity.class);
                    notificationBuilder.setContentTitle("Reto Diario Fallido!").setContentText("lo sentimos!!!");
                }
                //crear el PendingIntent para el Click (accion) en la notificacion (en este caso lanzar Activity: WinnerActivity o ChallengeActivity).
                pendingIntent = PendingIntent.getActivity(myContext, 0, myIntent, 0);
                notificationBuilder.setContentIntent(pendingIntent);

                notificationManager = (NotificationManager) myContext.getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(2, notificationBuilder.build());

                break;

            case AppUtil.ACTION_SERVICE_WEEKLY:

                rta = intent.getIntExtra(AppUtil.EXTRA_SERVICE_WEEKLY, -1);

                NotificationCompat.Builder notificationBuilderW = new NotificationCompat.Builder(myContext)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setVibrate(vibrationTime)
                        .setSound(uri)
                        .setAutoCancel(true); //para cerrar la notificacion una vez de presione sobre ella.

                NotificationManager notificationManagerW;//NotificationManager muestra la notificacion al usuario usando el metodo notify.

                if (rta == 1) {
                    myIntent = new Intent(myContext, WinnerActivity.class);
                    notificationBuilderW.setContentTitle("Reto Semanal cumplido!").setContentText("Felicidades...pulsa aquí!!!");
                } else if (rta == 2) {
                    myIntent = new Intent(myContext, MenuActivity.class);
                    notificationBuilderW.setContentTitle("Reto Semanal Fallido!").setContentText("lo sentimos!!!");
                }

                pendingIntent = PendingIntent.getActivity(myContext, 0, myIntent, 0);
                notificationBuilderW.setContentIntent(pendingIntent);

                notificationManagerW = (NotificationManager) myContext.getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManagerW.notify(4, notificationBuilderW.build());

                break;

            case AppUtil.ACTION_SERVICE_MONTHLY:

                rta = intent.getIntExtra(AppUtil.EXTRA_SERVICE_MONTHLY, -1);
                NotificationCompat.Builder notificationBuilderM = new NotificationCompat.Builder(myContext)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setVibrate(vibrationTime)
                        .setSound(uri)
                        .setAutoCancel(true); //para cerrar la notificacion una vez de presione sobre ella.

                NotificationManager notificationManagerM;//NotificationManager muestra la notificacion al usuario usando el metodo notify.

                if (rta == 1) {
                    myIntent = new Intent(myContext, WinnerActivity.class);
                    notificationBuilderM.setContentTitle("Reto Mensual cumplido!").setContentText("Felicidades...pulsa aquí!!!");
                } else if (rta == 2) {
                    myIntent = new Intent(myContext, MenuActivity.class);
                    notificationBuilderM.setContentTitle("Reto Mensual Fallido!").setContentText("lo sentimos!!!");
                }

                pendingIntent = PendingIntent.getActivity(myContext, 0, myIntent, 0);
                notificationBuilderM.setContentIntent(pendingIntent);

                notificationManagerM = (NotificationManager) myContext.getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManagerM.notify(6, notificationBuilderM.build());

                break;

            case AppUtil.ACTION_SERVICE_DAILY_INVITED:

                rta = intent.getIntExtra(AppUtil.EXTRA_SERVICE_DAILY_INVITED, -1);

                NotificationCompat.Builder nBDI = new NotificationCompat.Builder(myContext)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setVibrate(vibrationTime)
                        .setSound(uri)
                        .setAutoCancel(true);

                NotificationManager nMDI;
                if (rta == 1) {//se cumple el reto
                    myIntent = new Intent(myContext, WinnerActivity.class);
                    nBDI.setContentTitle("Reto Diario cumplido!").setContentText("Felicidades...pulsa aquí!!!");
                } else if (rta == 2) {
                    myIntent = new Intent(myContext, MenuActivity.class);
                    nBDI.setContentTitle("Reto Diario Fallido!").setContentText("lo sentimos!!!");
                } else if (rta == 3) {
                    //Debemos re lanzar el service para siempre monitorear cuando se ha cumplido o no un reto
                    intentChallenge = new Intent(myContext, DailyChallengeIntentService.class);
                    intentChallenge.setAction(AppUtil.ACTION_SERVICE_DAILY_INVITED);
                    intentChallenge.putExtra(AppUtil.EXTRA_SERVICE_DAILY_INVITED, DailyChallengeIntentService.DAILY_SERVICE_DELAY_INVITED);
                    myContext.startService(intentChallenge);
                }

                //crear el PendingIntent para el Click (accion) en la notificacion (en este caso lanzar Activity: WinnerActivity o ChallengeActivity).
                /*pendingIntent = PendingIntent.getActivity(myContext, 0, myIntent, 0);
                nBDI.setContentIntent(pendingIntent);*/
                if(rta != 3) {
                    //crear el PendingIntent para el Click (accion) en la notificacion (en este caso lanzar Activity: WinnerActivity o ChallengeActivity).
                    pendingIntent = PendingIntent.getActivity(myContext, 0, myIntent, 0);
                    nBDI.setContentIntent(pendingIntent);
                    nMDI = (NotificationManager) myContext.getSystemService(Context.NOTIFICATION_SERVICE);
                    nMDI.notify(8, nBDI.build());
                }

                break;

            case AppUtil.ACTION_SERVICE_WEEKLY_INVITED:

                rta = intent.getIntExtra(AppUtil.EXTRA_SERVICE_WEEKLY_INVITED, -1);

                NotificationCompat.Builder nBWI = new NotificationCompat.Builder(myContext)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setVibrate(vibrationTime)
                        .setSound(uri)
                        .setAutoCancel(true);

                NotificationManager nMWI;
                if (rta == 1) {//se cumple el reto
                    myIntent = new Intent(myContext, WinnerActivity.class);
                    nBWI.setContentTitle("Reto Semanal cumplido!").setContentText("Felicidades...pulsa aquí!!!");
                } else if (rta == 2) {
                    myIntent = new Intent(myContext, MenuActivity.class);
                    nBWI.setContentTitle("Reto Semanal Fallido!").setContentText("lo sentimos!!!");
                }else if (rta == 3) {
                    //Debemos re lanzar el service para siempre monitorear cuando se ha cumplido o no un reto
                    intentChallenge = new Intent(myContext, WeeklyChallengeIntentService.class);
                    intentChallenge.setAction(AppUtil.ACTION_SERVICE_WEEKLY_INVITED);
                    intentChallenge.putExtra(AppUtil.EXTRA_SERVICE_WEEKLY_INVITED, WeeklyChallengeIntentService.WEEKLY_SERVICE_DELAY_INVITED);
                    myContext.startService(intentChallenge);
                }

                if(rta != 3) {
                    //crear el PendingIntent para el Click (accion) en la notificacion (en este caso lanzar Activity: WinnerActivity o ChallengeActivity).
                    pendingIntent = PendingIntent.getActivity(myContext, 0, myIntent, 0);
                    nBWI.setContentIntent(pendingIntent);
                    nMWI = (NotificationManager) myContext.getSystemService(Context.NOTIFICATION_SERVICE);
                    nMWI.notify(10, nBWI.build());
                }

                break;

            case AppUtil.ACTION_SERVICE_MONTHLY_INVITED:

                rta = intent.getIntExtra(AppUtil.EXTRA_SERVICE_MONTHLY_INVITED, -1);

                NotificationCompat.Builder nBMI = new NotificationCompat.Builder(myContext)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setVibrate(vibrationTime)
                        .setSound(uri)
                        .setAutoCancel(true);

                NotificationManager nMMI;
                if (rta == 1) {//se cumple el reto
                    myIntent = new Intent(myContext, WinnerActivity.class);
                    nBMI.setContentTitle("Reto Mensual cumplido!").setContentText("Felicidades...pulsa aquí!!!");//.setContentIntent(pendingIntent);
                } else if (rta == 2) {
                    myIntent = new Intent(myContext, MenuActivity.class);
                    nBMI.setContentTitle("Reto Mensual Fallido!").setContentText("lo sentimos!!!");//.setContentIntent(pendingIntent);
                }else if (rta == 3) {
                    //Debemos re lanzar el service para siempre monitorear cuando se ha cumplido o no un reto
                    intentChallenge = new Intent(myContext, MonthlyChallengeIntentService.class);
                    intentChallenge.setAction(AppUtil.ACTION_SERVICE_MONTHLY_INVITED);
                    intentChallenge.putExtra(AppUtil.EXTRA_SERVICE_MONTHLY_INVITED, MonthlyChallengeIntentService.MONTHLY_SERVICE_DELAY_INVITED);
                    myContext.startService(intentChallenge);
                }

                if(rta != 3) {
                    //crear el PendingIntent para el Click (accion) en la notificacion (en este caso lanzar Activity: WinnerActivity o ChallengeActivity).
                    pendingIntent = PendingIntent.getActivity(myContext, 0, myIntent, 0);
                    nBMI.setContentIntent(pendingIntent);
                    nMMI = (NotificationManager) myContext.getSystemService(Context.NOTIFICATION_SERVICE);
                    nMMI.notify(12, nBMI.build());
                }

                break;

        }
    }
}
