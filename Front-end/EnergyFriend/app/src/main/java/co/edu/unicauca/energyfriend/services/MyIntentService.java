package co.edu.unicauca.energyfriend.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import co.edu.unicauca.energyfriend.R;
import co.edu.unicauca.energyfriend.activities.WinnerActivity;
import co.edu.unicauca.energyfriend.preferences.AppUtil;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class MyIntentService extends IntentService {


    float valueChallenge;
    public MyIntentService() {
        super("MyIntentService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {

            String action = intent.getAction();
            if (AppUtil.ACTION_SERVICE_DAILY.equals(action)) {
                valueChallenge = intent.getFloatExtra(AppUtil.EXTRA_SERVICE_DAILY,-1);
                handleActionRun();
            }

        }
    }

    private void handleActionRun() {
        try {

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    //.setLargeIcon(BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher))
                    .setContentTitle("Nuevo Reto Diario establecido!")
                    .setContentText("esperando...")
                    .setPriority(Notification.PRIORITY_MIN);


                startForeground(1, notificationBuilder.build());//establecer el service en primer plano
                Thread.sleep(10000); //esperamos 10 segundos
                Intent localIntent = new Intent(AppUtil.ACTION_SERVICE_DAILY).putExtra(AppUtil.EXTRA_SERVICE_DAILY, 5);
                LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);// Emisión de  localIntent
                Thread.sleep(300);//este retado parece ser que es necesario para que se reciba correctamente el dato antes de llamar a stopForeground().
                stopForeground(true); //quitamos del primer plano el service
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Toast.makeText(this,"Servicio Destruido!",Toast.LENGTH_SHORT).show();
        Log.d("mydser", "Servicio destruido...");
    }
}
