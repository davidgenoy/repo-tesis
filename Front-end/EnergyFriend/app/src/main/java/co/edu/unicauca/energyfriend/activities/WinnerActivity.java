package co.edu.unicauca.energyfriend.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.ShareApi;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;

import java.util.Arrays;
import java.util.List;

import co.edu.unicauca.energyfriend.R;
import co.edu.unicauca.energyfriend.preferences.AppUtil;

public class WinnerActivity extends AppCompatActivity implements View.OnClickListener {

    //Atributos
    public static final String UNIVERSAL_TAG = "stack";
    //public static final String IMAGE_URL = "https://goo.gl/D9Q5BE";
    TextView textViewTitle;

    private CallbackManager callbackManager;
    private LoginManager loginManager;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_winner);
        Log.d(UNIVERSAL_TAG,"ESTOY EN WinnerActivity");
        sharedPreferences = getSharedPreferences(AppUtil.PREFERENCE_NAME, MODE_PRIVATE);

        textViewTitle = (TextView) findViewById(R.id.txt_challenge_title);
        Button button = (Button) findViewById(R.id.btn_launch_app);

        //fcbk
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        List<String> permissionNeeds= Arrays.asList("publish_actions");
        loginManager= LoginManager.getInstance();
        //shareDialog= new ShareDialog(this);
        publishImage();
        loginManager.logInWithPublishPermissions(this,permissionNeeds);
        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                publishImage();
            }

            @Override
            public void onCancel() {
                System.out.println("onCanceled");
            }

            @Override
            public void onError(FacebookException e) {
                System.out.println("onError " + e);
            }
        });

        button.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent intentt = new Intent(this,MenuActivity.class);
        intentt.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//si ya hay una instancia de esta activity, limpia las q estan por encima de esta y se le pasa el Intent (por medio de  onNewIntent()) a esta activity que ahora esta en el Top del Back Stack
        startActivity(intentt);
        //startActivity(new Intent(this, MenuActivity.class));

    }

    private void publishImage() {
        //Bitmap image= BitmapFactory.decodeResource(getResources(), R.mipmap.publish_on_facebook);
        Bitmap image= BitmapFactory.decodeResource(getResources(), R.mipmap.green_earth);
        SharePhoto photo= new SharePhoto.Builder()
                .setBitmap(image)
                //.setImageUrl(Uri.parse(IMAGE_URL))
                .setCaption("Felicitaciones " + sharedPreferences.getString(AppUtil.USER_NAME,"") + " Has Cumplido el Reto de Ahorro Energético de Energy Friend ")
                .build();
        SharePhotoContent content= new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();
        ShareApi.share(content, null);
    }

    @Override
    protected void onActivityResult(final int requestCode,final int resultCode,final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    //escuchador cuando el usuario presiona el boton de atras
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this,MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//si ya hay una instancia de esta activity, limpia las q estan por encima de esta y se le pasa el Intent (por medio de  onNewIntent()) a esta activity que ahora esta en el Top del Back Stack
        intent.putExtra("EXIT",true);
        startActivity(intent);
        super.onBackPressed();
    }

}
