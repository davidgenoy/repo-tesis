package co.edu.unicauca.energyfriend.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import co.edu.unicauca.energyfriend.R;
import co.edu.unicauca.energyfriend.net.ManageRequest;

public class ConsumptionActivity extends AppCompatActivity implements View.OnClickListener, DialogInterface.OnCancelListener {

    //Atributos
    public static final String URL_KWH = "http://energyfriend.atwebpages.com/kwHrequest.php";
    public static final String UNIVERSAL_TAG = "stack";
    View v;
    TextView consumoPesos;
    AlertDialog.Builder builder;
    LayoutInflater inflater;
    Button btn_again, btn_ok;
    float kwHValue, kwHTotal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consumption);
        Log.d(UNIVERSAL_TAG,"ESTOY EN ConsumptionActivity");

        inflater = this.getLayoutInflater();
        v = inflater.inflate(R.layout.energy_consumption, null);

        builder = new AlertDialog.Builder(this, R.style.CustomDialog);
        builder.setView(v);
        consumoPesos = (TextView) v.findViewById(R.id.txt_consumption_value);

        ManageRequest.makeKwhRequest(this, URL_KWH, new ManageRequest.VolleyDataResponseListener() {
            @Override
            public void onResponseDataObject(JSONObject response) {
                try {
                    kwHValue = (float) response.getDouble("vKwH");
                    kwHTotal = (float) response.getDouble("totalKwH");
                    consumoPesos.setText("$ " + kwHTotal*kwHValue);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorDataObject(String message) {

            }
        });

        btn_again = (Button) v.findViewById(R.id.btn_again);
        btn_ok = (Button) v.findViewById(R.id.btn_ok);

        btn_again.setOnClickListener(this);
        btn_ok.setOnClickListener(this);

        //este escuchador es para volver a mostrar el alertdialog si el usuario pulsa x fuera de él
        builder.setOnCancelListener(this);

        builder.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_again:
                Intent intentt = new Intent(this,ConsumptionActivity.class);
                intentt.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//si ya hay una instancia de esta activity, limpia las q estan por encima de esta y se le pasa el Intent (por medio de  onNewIntent()) a esta activity que ahora esta en el Top del Back Stack
                startActivity(intentt);
                //startActivity(new Intent(this, ConsumptionActivity.class));
                break;
            case R.id.btn_ok:
                Intent intents = new Intent(this,MenuActivity.class);
                intents.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//si ya hay una instancia de esta activity, limpia las q estan por encima de esta y se le pasa el Intent (por medio de  onNewIntent()) a esta activity que ahora esta en el Top del Back Stack
                startActivity(intents);
                //startActivity(new Intent(this, MenuActivity.class));
                break;
        }
    }


    @Override
    public void onCancel(DialogInterface dialog) {
        v = inflater.inflate(R.layout.energy_consumption, null);
        builder.setView(v);
        btn_again = (Button) v.findViewById(R.id.btn_again);
        btn_ok = (Button) v.findViewById(R.id.btn_ok);
        consumoPesos = (TextView) v.findViewById(R.id.txt_consumption_value);
        consumoPesos.setText("$ " + kwHTotal*kwHValue);

        btn_again.setOnClickListener(this);
        btn_ok.setOnClickListener(this);
        builder.show();
    }

    //escuchador cuando el usuario presiona el boton de atras

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this,MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//si ya hay una instancia de esta activity, limpia las q estan por encima de esta y se le pasa el Intent (por medio de  onNewIntent()) a esta activity que ahora esta en el Top del Back Stack
        intent.putExtra("EXIT",true);
        startActivity(intent);
        super.onBackPressed();
    }
}
