package co.edu.unicauca.energyfriend;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import co.edu.unicauca.energyfriend.activities.MainActivity;
import co.edu.unicauca.energyfriend.preferences.AppUtil;

public class RootActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPreferences = getSharedPreferences(AppUtil.PREFERENCE_NAME, MODE_PRIVATE);
        Intent intent = null;
        intent = new Intent(this, MainActivity.class);
        if(sharedPreferences.getBoolean(AppUtil.USER_LOGIN_SESSION,false)){
            intent.putExtra("sesion", "s");
        }else{
            intent.putExtra("sesion", "n");
        }
        startActivity(intent);
        finish();
    }
}
