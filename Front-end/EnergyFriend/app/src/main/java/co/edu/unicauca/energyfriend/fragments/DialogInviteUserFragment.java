package co.edu.unicauca.energyfriend.fragments;


import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONException;
import org.json.JSONObject;

import co.edu.unicauca.energyfriend.R;
import co.edu.unicauca.energyfriend.net.ManageRequest;
import co.edu.unicauca.energyfriend.preferences.AppUtil;

import static android.content.Context.MODE_PRIVATE;
import static co.edu.unicauca.energyfriend.activities.MenuActivity.URL_TYPEUSER;

public class DialogInviteUserFragment extends Fragment {

    public static final int OPTION = 1;
    public static final int OPTION_MENU = 2;
    Context context;
    AlertDialog.Builder alertDialog;
    NavigationInterface navigationInterface;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;

    public DialogInviteUserFragment() {
    }

    public interface NavigationInterface {
        void navigationToFragment(int option);
    }

    public void init(Context context, NavigationInterface navigationInterface) {
        this.navigationInterface = navigationInterface;
        this.context = context;
        editor = context.getSharedPreferences(AppUtil.PREFERENCE_NAME, MODE_PRIVATE).edit();
        sharedPreferences = context.getSharedPreferences(AppUtil.PREFERENCE_NAME, MODE_PRIVATE);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_dialog_invite_user, null);

            //consultamos que tipo de usuario es
            ManageRequest.makeTypeUserRequest(context, URL_TYPEUSER, new ManageRequest.VolleyDataResponseListener() {
                @Override
                public void onResponseDataObject(JSONObject response) {
                    try {
                        if (response.getInt("tU") == 1) {//si el usuario es de tipo Admin
                            editor.putInt(AppUtil.TYPE_USER, 1);
                            //consultamos solo la primera vez si ya ha usado o no la App
                            if( !(sharedPreferences.getBoolean(AppUtil.OLD_USER,false)) ) {//sino la ha usado antes, entra a este if

                                alertDialog = new AlertDialog.Builder(context);
                                alertDialog.setTitle("¿Desea invitar personas a usar la aplicación?");
                                //alertDialog.setCancelable(false);//para que no se cierre el alertdialog si el usuario pulsa por fuera de él
                                alertDialog.setPositiveButton("Invitar", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        navigationInterface.navigationToFragment(OPTION);
                                    }
                                });
                                alertDialog.setNegativeButton("Más tarde", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                        navigationInterface.navigationToFragment(OPTION_MENU);
                                    }
                                });
                                //este escuchador es para volver a mostrar el alertdialog si el usuario pulsa x fuera de él
                                alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                    @Override
                                    public void onCancel(DialogInterface dialog) {
                                        alertDialog.show();
                                    }
                                });
                                alertDialog.show();
                                editor.putBoolean(AppUtil.OLD_USER, true);//cuando el admin vuelva a usar la app ya no se le despliega de nuevo esta interface
                            }else{//entra aqui si el admin ya ha usado la app antes
                                navigationInterface.navigationToFragment(OPTION_MENU);
                            }
                        } else {//el usuario es de tipo Invitado
                            editor.putInt(AppUtil.TYPE_USER, 2);
                            navigationInterface.navigationToFragment(OPTION_MENU);
                        }
                        editor.commit(); //confirmamos lo todos cambios anteriores (segun el sea el caso)
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onErrorDataObject(String message) {

                }
            });

        return v;
    }

}
